/*
 * keys.h
 *
 *  Created on: 07.11.2016
 *      Author: michael
 */

#ifndef LIB_KEYS_H_
#define LIB_KEYS_H_

//--------------------------------------------------------------
// Includes
//--------------------------------------------------------------
#include <stdio.h>
#include "stm32f10x.h"
#include "dwt.h"
#include "uart1.h"
#include "general.h"

#define KEY_A_PORT
#define KEY_B_PORT
#define KEY_C_PORT
#define KEY_D_PORT

#define KEY_A_PIN
#define KEY_B_PIN
#define KEY_C_PIN
#define KEY_D_PIN

#define KEY_A_BYTE 0b00000100 //0x04
#define KEY_B_BYTE 0b00001000 //0x08
#define KEY_C_BYTE 0b00010000 //0x10
#define KEY_D_BYTE 0b00100000 //0x20
#define KEY_E_BYTE 0b00000010 //0x01
#define KEY_F_BYTE 0b00000001 //0x02

void Keys_Processing(void);

#endif /* LIB_KEYS_H_ */
