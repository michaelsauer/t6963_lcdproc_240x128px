/*
 * t6963.c
 *
 *  Created on: 22.10.2016
 *      Author: michael
 */
#include "t6963.h"
#include "main.h"
#include "uart1.h"

#define T6963_DELAY 15

unsigned int _FW		= 6; //T6963_FONT_WIDTH;//Font Width
unsigned int _GH		= 0x00;//Graphics home
unsigned int _TH		= 0x00;	//Text Home
unsigned int _GA		= 0x28; //(T6963_PIXEL_HORIZONTAL / T6963_FONT_WIDTH);
unsigned int _TA		= 0x28; //(T6963_PIXEL_HORIZONTAL / T6963_FONT_WIDTH);

uint16_t sizeGA;
uint16_t sizeTA;

void T6963_Init_IO(void);
void T6963_Pin_Low(TLCD_NAME_t lcd_pin);
void T6963_Pin_High(TLCD_NAME_t lcd_pin);
char T6963_CheckStatus(void);
uint8_t constrain(uint8_t x, uint8_t a, uint8_t b);

/* T6963C Control Pins
	 * Write   PB9
	 * Read    PB4
	 * CE      PB5
	 * C/D     PB6
	 * Reset   PB10
	 * Reverse PB8
* T6963C Data Pins D0-D7 -> PA0-PA7 */
LCD_t LCD[] =
{
	// Name   ,PORT , PIN       , CLOCK              , Init
	{ TLCD_WR,  GPIOB, GPIO_Pin_9, 9, RCC_APB2Periph_GPIOB, Bit_SET },
	{ TLCD_RD,  GPIOB, GPIO_Pin_11, 11, RCC_APB2Periph_GPIOB, Bit_SET },
	{ TLCD_CE,  GPIOB, GPIO_Pin_5, 5, RCC_APB2Periph_GPIOB, Bit_SET },
	{ TLCD_CD,  GPIOB, GPIO_Pin_6, 6, RCC_APB2Periph_GPIOB, Bit_SET },
	{ TLCD_RST, GPIOB, GPIO_Pin_10, 10, RCC_APB2Periph_GPIOB, Bit_SET },
	{ TLCD_D0,  GPIOA, GPIO_Pin_0, 0, RCC_APB2Periph_GPIOA, Bit_RESET },
	{ TLCD_D1,  GPIOA, GPIO_Pin_1, 1, RCC_APB2Periph_GPIOA, Bit_RESET },
	{ TLCD_D2,  GPIOA, GPIO_Pin_2, 2, RCC_APB2Periph_GPIOA, Bit_RESET },
	{ TLCD_D3,  GPIOA, GPIO_Pin_3, 3, RCC_APB2Periph_GPIOA, Bit_RESET },
	{ TLCD_D4,  GPIOA, GPIO_Pin_4, 4, RCC_APB2Periph_GPIOA, Bit_RESET },
	{ TLCD_D5,  GPIOA, GPIO_Pin_5, 5, RCC_APB2Periph_GPIOA, Bit_RESET },
	{ TLCD_D6,  GPIOA, GPIO_Pin_6, 6, RCC_APB2Periph_GPIOA, Bit_RESET },
	{ TLCD_D7,  GPIOA, GPIO_Pin_7, 7, RCC_APB2Periph_GPIOA, Bit_RESET },
	{ TLCD_REV, GPIOB, GPIO_Pin_8, 8, RCC_APB2Periph_GPIOB, Bit_RESET },
	{ TLCD_BL,  GPIOC, GPIO_Pin_0, 0, RCC_APB2Periph_GPIOC, Bit_SET },
};

//--------------------------------------------------------------
// interne Funktion
// init aller IO-Pins
//--------------------------------------------------------------
void T6963_Init_IO(void)
{
	GPIO_InitTypeDef GPIO_InitStructure;

	TLCD_NAME_t lcd_pin;

	for (lcd_pin = 0; lcd_pin < TLCD_ANZ; lcd_pin++)
	{
		// Clock Enable
		RCC_APB2PeriphClockCmd(LCD[lcd_pin].TLCD_CLK, ENABLE);

		// Config als Digital-Ausgang
		GPIO_InitStructure.GPIO_Pin = LCD[lcd_pin].TLCD_PIN;
		GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP;
		GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
		GPIO_Init(LCD[lcd_pin].TLCD_PORT, &GPIO_InitStructure);

		// Default Wert einstellen
		if (LCD[lcd_pin].TLCD_INIT == Bit_RESET)
		{
			T6963_Pin_Low(lcd_pin);
		}
		else
		{
			T6963_Pin_High(lcd_pin);
		}
	}

	/* Input Pins Test */
	/*RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOB, ENABLE);
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_0 | GPIO_Pin_1 | GPIO_Pin_2;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AIN | GPIO_Mode_IPU;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_Init(GPIOB, &GPIO_InitStructure);*/
	//GPIO_SetBits(GPIOB, GPIO_Pin_0 | GPIO_Pin_1 | GPIO_Pin_2);
}

/* Initializes the LCD Display */
void T6963_Init(void)
{
	// init aller IO-Pins
	T6963_Init_IO();

	_TH = 0x1700;
	_TA = 0x28;
	_GH = 0x00;
	_GA = 0x28;

	sizeGA = 0x1400; //_GA * 128;					//Supercedes GLCD_GRAPHIC_SIZE
	sizeTA = 0x400; //_TA * (128 / 8);//0x280;	        	//Supercedes GLCD_TEXT_SIZE


	//Delay(15000000);

	T6963_Reset();

	T6963_SetDispMode(true, true, false, false);

	//Set Text home address
	T6963_WriteData(_TH & 0xFF);
	T6963_WriteData(_TH >> 8);
	T6963_WriteCommand(T6963_SET_TEXT_HOME_ADDRESS);

	//Set Text Area
	T6963_WriteData(_TA);
	T6963_WriteData(0x00);
	T6963_WriteCommand(T6963_SET_TEXT_AREA);

	//Set Graphics Home Address
	T6963_WriteData(_GH & 0xFF);
	T6963_WriteData(_GH >> 8);
	T6963_WriteCommand(T6963_SET_GRAPHIC_HOME_ADDRESS);

	//Set Graphics Area
	T6963_WriteData(_GA);
	T6963_WriteData(0x00);
	T6963_WriteCommand(T6963_SET_GRAPHIC_AREA);

	//Set Internal CGRAM address
	T6963_WriteData(T6963_OFFSET_REGISTER);
	T6963_WriteData(0x00);
	T6963_WriteCommand(T6963_SET_OFFSET_REGISTER);

	T6963_SetMode('O','I');

	T6963_ClearCG();
	T6963_ClearText();
	T6963_ClearGraphic();
}


//--------------------------------------------------------------
// interne Funktion
// Pin auf Lo setzen
//--------------------------------------------------------------
void T6963_Pin_Low(TLCD_NAME_t lcd_pin)
{
	LCD[lcd_pin].TLCD_PORT->BRR = (1 << LCD[lcd_pin].TLCD_PINNUM);
}

//--------------------------------------------------------------
// interne Funktion
// Pin auf Hi setzen
//--------------------------------------------------------------
void T6963_Pin_High(TLCD_NAME_t lcd_pin)
{
	LCD[lcd_pin].TLCD_PORT->BSRR = (1 << LCD[lcd_pin].TLCD_PINNUM);
}

void T6963_Reverse(bool state)
{
	if (state) { GPIO_SetBits(LCD[TLCD_REV].TLCD_PORT, LCD[TLCD_REV].TLCD_PIN); }
	else {GPIO_ResetBits(LCD[TLCD_REV].TLCD_PORT, LCD[TLCD_REV].TLCD_PIN); }
}

//--------------------------------------------------------------
// Reset des LCD Controllers
//--------------------------------------------------------------
void T6963_Reset(void)
{
	GPIO_ResetBits(GPIOB, GPIO_Pin_10);
	Delay(T6963_DELAY*100);
	GPIO_SetBits(GPIOB, GPIO_Pin_10);
}

//--------------------------------------------------------------
// Display Kommando schreiben
//--------------------------------------------------------------
void T6963_WriteCommand(uint8_t command)
{
  while(!(T6963_CheckStatus() & 0x03));
  LCD[TLCD_D0].TLCD_PORT->CRL = 0x33333333;
  LCD[TLCD_D0].TLCD_PORT->ODR = command;

  LCD[TLCD_WR].TLCD_PORT->BRR = (1 << LCD[TLCD_WR].TLCD_PINNUM) | (1 << LCD[TLCD_CE].TLCD_PINNUM);
  Delay(T6963_DELAY);
  LCD[TLCD_WR].TLCD_PORT->BSRR = (1 << LCD[TLCD_WR].TLCD_PINNUM) | (1 << LCD[TLCD_CE].TLCD_PINNUM);
}

//--------------------------------------------------------------
// Display Daten schreiben
//--------------------------------------------------------------
void T6963_WriteData(uint8_t data)
{
  while(!(T6963_CheckStatus() & 0x03));
  LCD[TLCD_D0].TLCD_PORT->CRL = 0x33333333;

  LCD[TLCD_D0].TLCD_PORT->ODR = data;

  LCD[TLCD_WR].TLCD_PORT->BRR = (1 << LCD[TLCD_WR].TLCD_PINNUM) | (1 << LCD[TLCD_CE].TLCD_PINNUM) | (1 << LCD[TLCD_CD].TLCD_PINNUM);
  Delay(T6963_DELAY);
  LCD[TLCD_WR].TLCD_PORT->BSRR = (1 << LCD[TLCD_WR].TLCD_PINNUM) | (1 << LCD[TLCD_CE].TLCD_PINNUM) | (1 << LCD[TLCD_CD].TLCD_PINNUM);
}

//--------------------------------------------------------------
// Display Daten lesen
//--------------------------------------------------------------
uint8_t T6963_ReadData(void)
{
  uint8_t tmp;
  while(!(T6963_CheckStatus() & 0x03));
  LCD[TLCD_D0].TLCD_PORT->CRL = 0x88888888;
  LCD[TLCD_RD].TLCD_PORT->BRR = (1 << LCD[TLCD_RD].TLCD_PINNUM) | (1 << LCD[TLCD_CE].TLCD_PINNUM) | (1 << LCD[TLCD_CD].TLCD_PINNUM);
  Delay(T6963_DELAY);
  tmp = LCD[TLCD_D0].TLCD_PORT->IDR;
  LCD[TLCD_RD].TLCD_PORT->BSRR = (1 << LCD[TLCD_RD].TLCD_PINNUM) | (1 << LCD[TLCD_CE].TLCD_PINNUM) | (1 << LCD[TLCD_CD].TLCD_PINNUM);
  LCD[TLCD_D0].TLCD_PORT->CRL = 0x33333333;
  return (tmp & 0xff);
}

//--------------------------------------------------------------
// Display Status lesen
//--------------------------------------------------------------
char T6963_CheckStatus(void)
{
	char tmp;
	Delay(T6963_DELAY);
	LCD[TLCD_D0].TLCD_PORT->CRL = 0x88888888;
	LCD[TLCD_RD].TLCD_PORT->BRR = (1<<LCD[TLCD_RD].TLCD_PINNUM) | (1<<LCD[TLCD_CE].TLCD_PINNUM);
	Delay(T6963_DELAY);
	tmp = LCD[TLCD_D0].TLCD_PORT->IDR;
	Delay(T6963_DELAY);
	LCD[TLCD_D0].TLCD_PORT->CRL = 0x33333333;
	LCD[TLCD_RD].TLCD_PORT->BSRR = (1<<LCD[TLCD_RD].TLCD_PINNUM) | (1<<LCD[TLCD_CE].TLCD_PINNUM);
	return (tmp & 0xff);
}

//-------------------------------------------------------------------------------------------------
//
// Sets address pointer for display RAM memory
//
//-------------------------------------------------------------------------------------------------
void T6963_SetAddressPointer(unsigned int address)
{
	T6963_WriteData(address & 0xFF);
	T6963_WriteData(address >> 8);
	T6963_WriteCommand(T6963_SET_ADDRESS_POINTER);
}

//-------------------------------------------------------------------------------------------------
//
// Clears text area of display RAM memory
//
//-------------------------------------------------------------------------------------------------
void T6963_ClearText()
{
	T6963_SetAddressPointer(_TH);
	for(int i = 0; i < sizeTA; i++){
		T6963_WriteDisplayData(0);
	}
}
//-------------------------------------------------------------------------------------------------
// Clears characters generator area of display RAM memory
//-------------------------------------------------------------------------------------------------
void T6963_ClearCG()
{
	//unsigned int i=((_sizeMem/2)-1)*0x800;
	T6963_SetAddressPointer(T6963_EXTERNAL_CG_HOME); //i);
	for(int i = 0; i < 256 * 8; i++){
		T6963_WriteDisplayData(0b00000000);
	}
}
//-------------------------------------------------------------------------------------------------
// Clears graphics area of display RAM memory
//-------------------------------------------------------------------------------------------------
void T6963_ClearGraphic()
{
	T6963_SetAddressPointer(_GH);
	for(unsigned int i = 0; i < sizeGA; i++){
		T6963_WriteDisplayData(0x00);
	}
}

//-------------------------------------------------------------------------------------------------
// Writes display data and increment address pointer
//-------------------------------------------------------------------------------------------------
void T6963_WriteDisplayData(uint8_t x)
{
	T6963_WriteData(x);
	T6963_WriteCommand(T6963_DATA_WRITE_AND_INCREMENT);
}

void T6963_WriteDataNon(uint8_t x)
{
	T6963_WriteData(x);
	T6963_WriteCommand(T6963_DATA_WRITE_AND_NONVARIALBE);
}

void T6963_WriteDataDec(uint8_t x)
{
	T6963_WriteData(x);
	T6963_WriteCommand(T6963_DATA_WRITE_AND_DECREMENT);
}

//-----------------------------------------------------------------------
//               Set Mode for Step 1 Text Manipulation and Show CGRam
//Example: LCD.setMode('0','I');
//
//Parameter: '0=Normal' 'X=Xor' 'A=And' 'T=Text Attribute Mode'
//			 , 'I=Intern CGram' , 'E=Extern CGram'
// Show Step 2 Example: (Set Text Attribute) to complete
//-----------------------------------------------------------------------
char T6963_SetMode(char _mode, char _CG)
{
	char tmp = T6963_MODE_SET;
	if(_mode=='X' || _mode =='x' || _mode=='^'){
		tmp |= 1;
	}
	else if(_mode == 'A' || _mode=='&' ||_mode=='a'){
		tmp |= 3;
	}
	else if(_mode == 'T'||_mode=='t'){
		tmp |=4;
	}
	else{
		tmp |= 0; //OR mode default
	}
	if(_CG =='E'||_CG=='e'){
		tmp |=8;
	}
	else{
		tmp |=0;
	}
	T6963_WriteCommand(tmp);
	return tmp;
}

//-----------------------------------------------------------------------
//                       Set Display Mode
//Example: LCD.setDispMode(true,true,false,false);    //true=ON	false=off
//
//Parameter: ('GRAPHIC', 'TEXT', 'CURSOR', 'CURSOR BLINK')
//
//Set blinking Cursor Example: 	LCD.setDispMode(true,true,true,true); //Mode all ON
//								LCD.setCursorPattern(8); // Cursor high
//								LCD.setCursorPointer(0,0); //Cursor Position
//-----------------------------------------------------------------------
char T6963_SetDispMode(bool _text, bool _graphics, bool _cursor, bool _blink)
{
  char tmp = T6963_DISPLAY_MODE;
  if(_graphics){
    tmp |= 0b1000; //T6963_GRAPHIC_DISPLAY_ON
  }
  if(_text){
    tmp |= 0b0100; //T6963_TEXT_DISPLAY_ON
  }
  if(_cursor){
    tmp |= 0b0010; //T6963_CURSOR_DISPLAY_ON
  }
  if(_blink){
    tmp |= 0b0001; //T6963_CURSOR_BLINK_ON
  }
  T6963_WriteCommand(tmp);
  return tmp;
}

//-------------------------------------------------------------------------------------------------
// Sets display coordinates
//-------------------------------------------------------------------------------------------------
void T6963_TextGoTo(unsigned char x, unsigned char y)
{
  unsigned int address;
  address = _TH + x + (_TA * y); //_TH
  T6963_SetAddressPointer(address);
}

//-------------------------------------------------------------------------------------------------
// Writes null-terminated string to display RAM memory
//
// Example: LCD.TextGoTo(0,3); //Position
//			LCD.writeString("Hello World"); Print String "Hello World"
//-------------------------------------------------------------------------------------------------
void T6963_WriteString(char * string)
{
  while(*string){
	T6963_WriteChar(*string++);
  }
}

//-------------------------------------------------------------------------------------------------
// Writes a single character (ASCII code) to display RAM memory
//-------------------------------------------------------------------------------------------------
void T6963_WriteChar(char charCode)
{
  T6963_WriteDisplayData(charCode - 32);
}

void T6963_WriteCharDec(char charCode)
{
	T6963_WriteDataDec(charCode - 32);
}

void T6963_WriteCGChar(unsigned char c)
{
	T6963_WriteDisplayData(0x80 + c);
	T6963_WriteCommand(T6963_DATA_WRITE_AND_INCREMENT);
}

void T6963_CreateLine(int x0, int y0, int x1, int y1, char color)
{
/*BreshenhamLine algorithm - From wikipedia so it must be right
http://en.wikipedia.org/wiki/Bresenham's_line_algorithm
*/
  bool steep = abs(y1 - y0) > abs(x1 - x0);
  if(steep){
    //swap(x0, y0)
    //swap(x1, y1)
    int tmp=x0;
    x0=y0;
    y0=tmp;
    tmp=x1;
    x1=y1;
    y1=tmp;
  }
  if(x0 > x1){
    //swap(x0, x1)
    //swap(y0, y1)
    int tmp=x0;
    x0=x1;
    x1=tmp;
    tmp=y0;
    y0=y1;
    y1=tmp;
  }
  int deltax = x1 - x0;
  int deltay = abs(y1 - y0);
  int error = deltax / 2;
  int ystep=-1;
  int y = y0;
  if(y0 < y1){ystep= 1;}
  for(int x =x0;x<=x1;x++){
    if(steep){ T6963_WritePixel(y,x,color); } else { T6963_WritePixel(x,y,color); }
    error = error - deltay;
    if(error < 0){
      y = y + ystep;
      error = error + deltax;
    }
  }
}

//-------------------------------------------------------------------------------------------------
// Set (if color==1) or clear (if color==0) pixel on screen
//-------------------------------------------------------------------------------------------------
void T6963_WritePixel(uint8_t x, uint8_t y, uint8_t color)
{
  unsigned char tmp;
  unsigned int address;
  address = _GH + (x / _FW) + (_GA * y);
  T6963_SetAddressPointer(address);
  T6963_WriteCommand(T6963_DATA_READ_AND_NONVARIABLE);
  tmp = T6963_ReadData();
  if(color){
    tmp |= (1 <<  (_FW - 1 - (x % _FW)));
  }
  else{
    tmp &= ~(1 <<  (_FW - 1 - (x % _FW)));
  }
  T6963_WriteDisplayData(tmp);
}

//-------------------------------------------------------------------------------------------------
// Set a single pixel at x,y (in pixels) to 1 (on)
//-------------------------------------------------------------------------------------------------
uint8_t T6963_SetPixel(uint8_t x, uint8_t y)
{
	T6963_SetAddressPointer((_GH + (x / _FW) + (_GA * y)));
	uint8_t tmp = 0b11111000;
	tmp |= (_FW-1)-(x%_FW); //LSB Direction Correction
	T6963_WriteCommand(tmp);
	return tmp;
}

//-------------------------------------------------------------------------------------------------
// Set a single pixel at x,y (in pixels) to 0 (off)
//-------------------------------------------------------------------------------------------------
uint8_t T6963_ClearPixel(uint8_t x, uint8_t y)
{
	T6963_SetAddressPointer((_GH + (x / _FW) + (_GA * y)));
	uint8_t tmp = 0b11110000;
	tmp |= (_FW-1)-(x%_FW); //LSB Direction Correction
	T6963_WriteCommand(tmp);
	return tmp;
}

//-------------------------------------------------------------------------------------------------
//                               Show Circle
////Example: LCD.createCircle(30, 30, 30, 1);
//
//Parameter: createCircle(int cx, int cy, int radius, color) //cx and cy mark the distance from the origin point.
//
//-------------------------------------------------------------------------------------------------

void T6963_CreateCircle(int cx, int cy, int radius, uint8_t color)
{
	int error = -radius;
	int x = radius;
	int y = 0;

	while (x >= y){
		T6963_Plot8Points(cx, cy, x, y, color);

		error += y;
		++y;
		error += y;

		if (error >= 0){
			--x;
			error -= x;
			error -= x;
		}
	}
}

void T6963_Plot8Points(int cx, int cy, int x, int y, uint8_t color)
{
	T6963_Plot4Points(cx, cy, x, y, color);
	if (x != y) T6963_Plot4Points(cx, cy, y, x, color);
}

void T6963_Plot4Points(int cx, int cy, int x, int y, uint8_t color)
{
	T6963_WritePixel(cx + x, cy + y, color);
	if (x != 0) T6963_WritePixel(cx - x, cy + y, color);
	if (y != 0) T6963_WritePixel(cx + x, cy - y, color);
	if (x != 0 && y != 0) T6963_WritePixel(cx - x, cy - y, color);
}

//-----------------------------------------------------------------------
//                               Set Text Attribute (Text only) (Step 2)
//Example: LCD.setMode('T','I');
//         LCD.setTextAttrMode('0');
//
//Parameter: '0=Normal display' '5=Reverse display' '3=Inhibit display'
//			 '8=Blink of normal display' 'D=Blink of reverse display'
//			 'B=Blink of inhibit display'
//-----------------------------------------------------------------------
uint8_t T6963_SetTextAttrMode(char _mode)
{   // Text only: 0=Normal display 5=Reverse display 3=Inhibit display 8=Blink of normal display D=Blink of reverse display B=Blink of inhibit display
	uint8_t tmp = 0;
	if(_mode=='3'){
		tmp |= 0b0011;
	}
	else if(_mode == '5'){
		tmp |= 0b0101;
	}
	else if(_mode == '8'){
		tmp |=0b1000;
	}
	else if(_mode == 'D'||_mode=='d'){
		tmp |=0b1101;
	}
	else if(_mode == 'B'||_mode=='b'){
		tmp |=0b1011;
	}
	else{
		tmp |= 0; //Normal Text mode default
	}
	T6963_SetAddressPointer(_GH);
	for(unsigned int i = 0; i < sizeGA; i++){
		T6963_WriteDisplayData(tmp);
	}
	return tmp;
}
//-----------------------------------------------------------------------

void T6963_ClearDispMode()
{
	T6963_WriteCommand(T6963_DISPLAY_MODE);
}

//-------------------------------------------------------------------------------------------------
//                               Show Rectangel (writePixel)
////Example: LCD.Rectangle (0, 0, 30, 20, 1);
//
//Parameter: Rectangle(unsigned char x, unsigned char y, unsigned char b, unsigned char a, color)    x1 and y1 top left Position b and a Size of the rectangle
//-------------------------------------------------------------------------------------------------
void T6963_Rectangle(unsigned char x, unsigned char y, unsigned char b, unsigned char a, uint8_t color)
{
	unsigned char j;
	// Draw vertical lines
	for (j = 0; j < a; j++) {
		T6963_WritePixel(x, y + j, color);
		T6963_WritePixel(x + b - 1, y + j, color);
	}
	// Draw horizontal lines
	for (j = 0; j < b; j++)	{
		T6963_WritePixel(x + j, y, color);
		T6963_WritePixel(x + j, y + a - 1, color);
	}
}

//-------------------------------------------------------------------------------------------------
//                               Show filled Box (WritePixel)
////Example: LCD.fillRect (0,0,50,10,1);
//
//Parameter: fillRect (int x1, int y1, int x2, int y2, color) x1 and y1 top left Position  x2 and y2 bottom right Position
//-------------------------------------------------------------------------------------------------
void T6963_FillRect(int x1, int y1, int x2, int y2, uint8_t color)
{
	for (int i = y1; i <= y2; i++)
		T6963_CreateLine(x1, i, x2, i, color);
}

//-------------------------------------------------------------------------------------------------
//                               Show filled Box (BYTE)
////Example: LCD.setDispMode(true,true,false,false);    //true=ON	false=off
//
//Parameter: ('GRAPHIC', 'TEXT', 'CURSOR', 'CURSOR BLINK')
//
// drawrectbyte(x, y, height, bytewidth, pattern) // x,y = Start Position, Height in Pixel, Widht in Byte, pattern in byte(0b11111111)
//-------------------------------------------------------------------------------------------------
void T6963_Drawrectbyte(unsigned int x, unsigned int y, unsigned int HEIGHT, unsigned int BYTEWIDTH, uint8_t pattern)    //HEIGHT = max 64;	//Height in pixels     BYTEWIDTH = max 40;	//Width in bytes by 6x8 Font
{
	int j;

	T6963_GraphicGoTo(x,y);
	j=0;

	for(int h = 0; h < HEIGHT; h++)
	{
		T6963_GraphicGoTo(x,y+h);

		for(unsigned int i = 0; i < BYTEWIDTH; i++)     //(PIXELS PER LINE/FONT WIDTH * NUMBER OF LINES)
		{
			T6963_WriteDisplayData(pattern);
		}
		j=j+BYTEWIDTH;
	}
}

//-----------------------------------------------------------------------
//                               Show Picture(beta)
//Generate Picture File  by "BMP2C.exe datei.bmp 6" example for 6*8 Pixel
//http://www.holger-klabunde.de
//6 pixel per byte for T6963 and 6x8 font
//Horizontal pixel orientation for T6963
//Pixel orientation per Byte: D7..D0
//
//Example: #include <demo.h>									//Filename
//		   LCD.drawPic( 0, 0, demobmp ,demoHEIGHT, demoBYTEWIDTH );; //Show Demo Picture

// Parameter :  x,y = Position, Picturename (not Filename see in Picturefile), Height in Pixel, Widht in Byte
//-----------------------------------------------------------------------
void T6963_DrawPic(unsigned int x, unsigned int y, unsigned char * PicName, unsigned int HEIGHT, unsigned int BYTEWIDTH)
 //HEIGHT = max 64;	//Height in pixels     BYTEWIDTH = max 40;	//Width in bytes by 6x8 Font
{
	uint8_t bitmap;
	int j;

	j=0;
	for(int h = 0; h < HEIGHT; h++)
	{
		T6963_GraphicGoTo(x,y+h);

		for(int i = 0; i < BYTEWIDTH; i++)     //(PIXELS PER LINE/FONT WIDTH * NUMBER OF LINES)
		{
			bitmap = PicName[i+j];
			T6963_WriteDisplayData(bitmap);
		}
		j = j+BYTEWIDTH;
	}
}

void T6963_DrawAnim(unsigned int x, unsigned int y, unsigned char** string_table, int l, unsigned int HEIGHT, unsigned int BYTEWIDTH)
 //Position x, Position y, String of Images , Number of Images ,HEIGHT,BYTEWIDTH show bmp2c Picturefile
 {
	uint8_t bitmap;
	int j;
	char* PicName;

	for (int a = 0; a < l; a++)
	{
		PicName = (char*) string_table[a];
		j=0;

		for(int h = 0; h < HEIGHT-1; h++)
		{
			T6963_GraphicGoTo(x,y+h);

			for(unsigned int i = 0; i < BYTEWIDTH; i++)     //(PIXELS PER LINE/FONT WIDTH * NUMBER OF LINES)
			{
				bitmap = PicName[i+j];
				T6963_WriteDisplayData(bitmap);
			}
			j=j+BYTEWIDTH;
		}
	}
 }

//-------------------------------------------------------------------------------------------------
// Print String "Hello World" with Font (byte) faster
//
// Example: LCD.TextGoTo(0,0); //Position
//	           LCD.glcd_print2_P(0,0, "Hello World", &Segoe_Script__14,1);
//-------------------------------------------------------------------------------------------------
void T6963_Glcd_print2_P(unsigned char x,unsigned char y, const char *in, const struct FONT_DEF *strcut1, unsigned char invers)
{
	unsigned int offset,swbyte=0;
	unsigned char width, by=0, sbyte=0, mask=0, tmp=0;
	unsigned char NrBytes;
	unsigned char i,j,map,height,Fontwidth=0;

	while((map = *in++))
	{
		map = strcut1->mapping_table[map];

		width = strcut1->glyph_width;
		if(width == 0)
			width = strcut1->width_table[map];

                NrBytes = ((width-1)/8)+1;

		offset = strcut1->offset_table[map];
		height = strcut1->glyph_height;

       		for(j=0; j<height*NrBytes; j+=NrBytes)      // height
				{
                   swbyte=0;
                   T6963_GraphicGoTo(x+Fontwidth,y+j/NrBytes); //Graphics Pointer

				for(i=0; i<NrBytes; i++)            //  width
                    {
                    by = strcut1->glyph_table[offset+j+i]; //Read 8bit
                        if( _FW == 6)  //fontWidth
                        {
                             switch(swbyte)         //convert 8bit to 6bit Data for 6bit Font
                            {
							 case 0:                //First Byte
                             sbyte=by>>2;           //First 6 Bit out
                             tmp=by<<4;             //Store last 2 Bits from first Byte
                             swbyte++;
                             break;
                             case 1:               //First Byte and second Byte
                             sbyte=tmp+(by>>4);    //Second 6 Bit out
                             tmp=by<<2;            //Store last 4 Bits from second Byte
                             swbyte++;
                             break;
                             case 2:               //Second and third Byte
                             sbyte=tmp+(by>>6);    //Third 6 Bit out
                             tmp=by;               //Store last 6 Bits from third Byte
                             swbyte++;
                             break;
                             case 3:               //last 6 bits from the third byte
                             sbyte=tmp;            //Fourth 6 Bit out
                             i-=1;                 //i Counter correction while read by
                             swbyte=0;
                             break;
                            }                    //End convert 8bit to 6bit Font
                        T6963_WriteDisplayData(sbyte); //6bit Font
                        }
                        else
                        	T6963_WriteDisplayData(by);  // 8bit Font
					}//End i
                }// End j
		Fontwidth+=_FW*NrBytes;
	}// End K

}
//-------------------------------------------------------------------------------------------------
// Print String "Hello World" with Font (Pixel) slower
//
// Example: LCD.TextGoTo(0,0); //Position
//		LCD.glcd_print1_P(0,0, "Hello World", &Segoe_Script__14,1);
//-------------------------------------------------------------------------------------------------
void T6963_Glcd_print1_P(unsigned char x,unsigned char y, const char *in, const struct FONT_DEF *strcut1, unsigned char invers)
{
	unsigned int offset;
	unsigned char width, by=0, mask=0;
	unsigned char NrBytes;
	unsigned char i,j,map,height,allwidth=0;

	while((map = *in++))
	{
		map = strcut1->mapping_table[map];

		width = strcut1->glyph_width;
		if(width == 0)
			width = strcut1->width_table[map];

		offset = strcut1->offset_table[map];
		height = strcut1->glyph_height;

        NrBytes = ((width-1)/8)+1;

		for(j=0; j<height * NrBytes; j+=NrBytes    )
		{   // height
			for(i=0 ; i<width  ; i++)
			{   //  width
			    if(i%8 == 0)
			    {
			     by = strcut1->glyph_table[ offset+j+(i/8) ];
			     mask = 0x80;
			    }

				if( by & mask )
					T6963_WritePixel(  x+i+allwidth , y+j/ NrBytes, !invers  );
	 			else
	 				T6963_WritePixel(  x+i+allwidth , y+j/ NrBytes, invers  );

	 			mask >>= 1;
			}//End i
		}// End j
		allwidth+=width;
	}// End K
}

 //-------------------------------------------------------------------------------------------------
 // Sets graphics coordinates
 //-------------------------------------------------------------------------------------------------
 void T6963_GraphicGoTo(uint8_t x, uint8_t y)
 {
	unsigned int address;
	address = _GH + (x / _FW) + (_GA * y);
	T6963_SetAddressPointer(address);
 }

 //-------------------------------------------------------------------------------------------------
 // Writes single char pattern to character generator area of display RAM memory
 //-------------------------------------------------------------------------------------------------
unsigned int T6963_DefineCharacter(uint8_t charCode, unsigned char * defChar)
 {
	//unsigned int address=(((_sizeMem/2)-1)* 0x800) + (unsigned int)charCode*8;
	unsigned int address = (unsigned int)T6963_EXTERNAL_CG_HOME + ((unsigned int)charCode * 8);
	T6963_SetAddressPointer(address);
	for(uint8_t i = 0; i < 8 ; i++){
		T6963_WriteDisplayData(defChar[i]);
	}
	return address;
 }

uint8_t constrain(uint8_t x, uint8_t a, uint8_t b)
{
    if(x < a) {
        return a;
    }
    else if(b < x) {
        return b;
    }
    else
        return x;
}


//-------------------------------------------------------------------------------------------------
//
// Cursor Controls
//
//-------------------------------------------------------------------------------------------------
uint8_t T6963_SetCursorPattern(uint8_t _b)
{
	uint8_t tmp = T6963_CURSOR_PATTERN_SELECT;
	_b = constrain(_b,0,7);
	tmp |= _b;
	T6963_WriteCommand(tmp);
	return tmp;
}

void T6963_SetCursorPointer(uint8_t _col, uint8_t _row)
{
	_col=constrain(_col,0,(_TA-1));
	_row=constrain(_row,0,((128/8)-1));
	T6963_WriteData(_col);
	T6963_WriteData(_row);
	T6963_WriteCommand(T6963_SET_CURSOR_POINTER); //Cursor pointer Set
}

void T6963_Backlight(bool state)
{
	if (state) {
		GPIO_ResetBits(GPIOC, GPIO_Pin_0);
	} else {
		GPIO_SetBits(GPIOC, GPIO_Pin_0);
	}
}


