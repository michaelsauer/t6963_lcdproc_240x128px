/**
 * @file uart1.c
 *
 * @brief This file includes the UART1 functions
 *	      It handles ML Protocol CRC Check and
 *	      general parsing of the MagicLight Protocol.
 *
 * @author Michael Sauer <sauer.uetersen@gmail.com>
 *
 * @date 14.09.2014 - erste Version
 *
 * @note Packet Format:
 * STX | LEN | ADR | DATA | BCC | 0x0D (\\r) | 0x0A (\\n)<br>
 * <br>
 * Adress Byte wird wie folgt zusammengesetzt:<br><pre>
 * Bit 7  6  5  4  3  2  1  0
 *                 |--|--|--|--- Typ des Telegramms (1=General / 2=Effektliste / 3= Einstellungen / ...) (one nibble)
 *              |.-------------- Fehler (Bit 4)
 *           |------------------ End condition (Bit 5)
 *        |--------------------- Start Condition (Bit 6)
 *     |------------------------ Antwort auf Anfrage oder Broadcast (1= Antwort / 0= Broadcast) (Bit 7)
 *</pre>
 */

#include <uart3.h>
#include "general.h"
#include "uart1.h"
#include "dwt.h"
#include "esp8266.h"

/** @addtogroup Libraries
 * @{
 */

/** @addtogroup UART3_Library
 * @{
 */

/** @addtogroup UART3_Exported_Defines
 * @{
 */
/**
 * @}
 */

/** @addtogroup UART3_Exported_Variables
 * @{
 */
/**
 * @}
 */

/** @addtogroup UART3_Exported_Functions
 * @{
 */

/**
 * @brief  Initializes USART3
 *
 * @param  baudrate	Baudrate to use
 * @retval None
 */
void USART3_Init(uint32_t baudrate)
{
	GPIO_InitTypeDef GPIO_InitStructure;
	USART_InitTypeDef USART_InitStructure;
	NVIC_InitTypeDef NVIC_InitStructure;

	/* Clock configuration -------------------------------------------------------*/
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_USART3, ENABLE);
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOC, ENABLE);
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_AFIO, ENABLE);

	GPIO_PinRemapConfig(GPIO_PartialRemap_USART3, ENABLE);

	NVIC_PriorityGroupConfig(NVIC_PriorityGroup_4);

	NVIC_InitStructure.NVIC_IRQChannel = USART3_IRQn;
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0x0F;
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0x0F;
	NVIC_Init(&NVIC_InitStructure);

	/* Configure the GPIO ports( USART1 Transmit and Receive Lines) */
	/* Configure the USART3_Tx as Alternate function Push-Pull */
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_PP;
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_10;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_Init(GPIOC, &GPIO_InitStructure);

	/* Configure the USART3_Rx as input floating */
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN_FLOATING;
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_11;
	GPIO_Init(GPIOC, &GPIO_InitStructure);



	/* USART3 configuration ------------------------------------------------------*/
	/* USART3 configured as follow:
	 - BaudRate = like parameter (115200kbps)
	 - Word Length = 9 Bits
	 - One Stop Bit
	 - Even parity
	 - Hardware flow control disabled (RTS and CTS signals)
	 - Receive and transmit enabled
	 */
	USART_InitStructure.USART_BaudRate = baudrate;
	USART_InitStructure.USART_WordLength = USART_WordLength_8b;
	USART_InitStructure.USART_StopBits = USART_StopBits_1;
	USART_InitStructure.USART_Parity = USART_Parity_No;
	USART_InitStructure.USART_HardwareFlowControl = USART_HardwareFlowControl_None;
	USART_InitStructure.USART_Mode = USART_Mode_Rx | USART_Mode_Tx;

	USART_DeInit(USART3);

	/* Configure the USART3 */
	USART_Init(USART3, &USART_InitStructure);

	/* Enable USART3 interrupt */
	USART_ITConfig(USART3, USART_IT_RXNE, ENABLE);
	//USART_ITConfig(USART3, USART_IT_TXE,ENABLE);

	/* Enable the USART3 */
	USART_Cmd(USART3, ENABLE);
}

/**
 * @brief  Puts a byte to USART3
 *
 * @param  ch		Byte to send
 * @retval None
 */
void USART3_Put(uint8_t ch)
{
	USART_SendData(USART3, (uint8_t) ch);
	//Loop until the end of transmission or timeout appears
	while (USART_GetFlagStatus(USART3, USART_FLAG_TC) == RESET){};
}

/**
 * @brief  Gets one byte from USART3
 *
 * @retval None
 */
uint8_t USART3_Get(void)
{
	while (USART_GetFlagStatus(USART3, USART_FLAG_RXNE) == RESET){};
	return (uint8_t) USART_ReceiveData(USART3);
}

/**
 * @brief  Put string to USART3
 *
 * @param  s		String to send
 * @retval None
 */
void USART3_Puts(volatile char *s)
{
	while (*s)
	{
		// wait until data register is empty
		while (!(USART3->SR & 0x00000040)) {};
		USART_SendData(USART3, *s++);
	}
}

void USART3_Send(uint8_t* DataArray, uint16_t count)
{
	/* Go through entire data array */
	while (count--) {
		/* Wait to be ready, buffer empty */
		while (!(USART3->SR & 0x00000040)) {};
		/* Send data */
		USART3->DR = (uint16_t)(*DataArray++);
		/* Wait to be ready, buffer empty */
		while (!(USART3->SR & 0x00000040)) {};
	}
}

/**
 * @brief  This is the interrupt request handler (IRQ) for ALL USART3 interrupts
 *
 * @retval None
 */
void USART3_IRQHandler(void)
{
	// check if the USART3 receive interrupt flag was set
	if (USART_GetITStatus(USART3, USART_IT_RXNE)) {
		// get the new byte:
		char inChar = (uint8_t) USART_ReceiveData(USART3);
		//USART1_Put(inChar);
		ESP8266_DataReceived(&inChar, 1);
	}
}

/**
 * @}
 */

/**
 * @}
 */

/**
 * @}
 */
