/**
 * @file adc.h
 *
 * @brief This file includes the ADC functions prototypes
 *
 * @author Michael Sauer <sauer.uetersen@gmail.com>
 *
 * @date 20.10.2014 - erste Version
 */

#ifndef ADC_H_
#define ADC_H_

#include "stm32f10x.h"

/** @addtogroup Libraries
  * @{
  */

/** @addtogroup ADC_Library
  * @{
  */

/** @addtogroup ADC_Exported_Types
  * @{
  */
typedef enum {
	FILTER_LOW = 0,
	FILTER_MEDIUM = 1,
	FILTER_HIGH = 2,
	FILTER_REF = 3,
	FILTER_TEMP = 4
} ADC_FILTER;
/**
  * @}
  */

/** @addtogroup ADC_Exported_Functions_Prototypes
  * @{
  */
void ADC12_Init(void);
uint16_t ADC_GetValue(ADC_FILTER filter);
long ADC_Map(long x, long in_min, long in_max, long out_min, long out_max);
/**
  * @}
  */

/**
  * @}
  */

/**
  * @}
  */

#endif /* ADC_H_ */
