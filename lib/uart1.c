/**
 * @file uart1.c
 *
 * @brief This file includes the UART1 functions
 *	      It handles ML Protocol CRC Check and
 *	      general parsing of the MagicLight Protocol.
 *
 * @author Michael Sauer <sauer.uetersen@gmail.com>
 *
 * @date 14.09.2014 - erste Version
 *
 * @note Packet Format:
 * STX | LEN | ADR | DATA | BCC | 0x0D (\\r) | 0x0A (\\n)<br>
 * <br>
 * Adress Byte wird wie folgt zusammengesetzt:<br><pre>
 * Bit 7  6  5  4  3  2  1  0
 *                 |--|--|--|--- Typ des Telegramms (1=General / 2=Effektliste / 3= Einstellungen / ...) (one nibble)
 *              |.-------------- Fehler (Bit 4)
 *           |------------------ End condition (Bit 5)
 *        |--------------------- Start Condition (Bit 6)
 *     |------------------------ Antwort auf Anfrage oder Broadcast (1= Antwort / 0= Broadcast) (Bit 7)
 *</pre>
 */

#include "uart1.h"
#include "dwt.h"
#include "uart3.h"

/** @addtogroup Libraries
 * @{
 */

/** @addtogroup UART1_Library
 * @{
 */

/** @addtogroup UART1_Exported_Defines
 * @{
 */
/**
 * @}
 */

/** @addtogroup UART1_Exported_Variables
 * @{
 */
unsigned long packetTimer;
bool packetTimeout;
/**
 * @}
 */

/** @addtogroup UART1_Exported_Functions
 * @{
 */

/**
 * @brief  Logs Message to USART1 with Verbose Level
 *
 * @param  level	Verbose Level to use
 * @param	msg		Message to Send
 * @retval None
 *
 * @note	Add \\r\\n for breaks
 */
void Log(uint8_t level, char* msg)
{
	if (level <= VERBOSE)
		printf("[%d] %s", level, msg);
}

/**
 * @brief  Initializes USART1
 *
 * @param  baudrate	Baudrate to use
 * @retval None
 */
void USART1_Init(uint32_t baudrate)
{
	GPIO_InitTypeDef GPIO_InitStructure;
	USART_InitTypeDef USART_InitStructure;
	NVIC_InitTypeDef NVIC_InitStructure;

	/* Clock configuration -------------------------------------------------------*/
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_USART1, ENABLE);
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA, ENABLE);

	NVIC_PriorityGroupConfig(NVIC_PriorityGroup_4);

	NVIC_InitStructure.NVIC_IRQChannel = USART1_IRQn;
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0x0F;
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0x0F;
	NVIC_Init(&NVIC_InitStructure);

	//GPIO_PinRemapConfig(GPIO_Remap_USART1, ENABLE);


	/* Configure the GPIO ports( USART1 Transmit and Receive Lines) */
	/* Configure the USART1_Tx as Alternate function Push-Pull */
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_PP;
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_9;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_Init(GPIOA, &GPIO_InitStructure);

	/* Configure the USART1_Rx as input floating */
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN_FLOATING;
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_10;
	GPIO_Init(GPIOA, &GPIO_InitStructure);



	/* USART1 configuration ------------------------------------------------------*/
	/* USART1 configured as follow:
	 - BaudRate = like parameter (115200kbps)
	 - Word Length = 9 Bits
	 - One Stop Bit
	 - Even parity
	 - Hardware flow control disabled (RTS and CTS signals)
	 - Receive and transmit enabled
	 */
	USART_InitStructure.USART_BaudRate = baudrate;
	USART_InitStructure.USART_WordLength = USART_WordLength_8b;
	USART_InitStructure.USART_StopBits = USART_StopBits_1;
	USART_InitStructure.USART_Parity = USART_Parity_No;
	USART_InitStructure.USART_HardwareFlowControl = USART_HardwareFlowControl_None;
	USART_InitStructure.USART_Mode = USART_Mode_Rx | USART_Mode_Tx;

	USART_DeInit(USART1);

	/* Configure the USART1 */
	USART_Init(USART1, &USART_InitStructure);

	/* Enable USART1 interrupt */
	USART_ITConfig(USART1, USART_IT_RXNE, ENABLE);
	//USART_ITConfig(USART1, USART_IT_TXE,ENABLE);

	/* Enable the USART1 */
	USART_Cmd(USART1, ENABLE);

	//packetTimer = 0;
	//packetTimeout = false;
}

/**
 * @brief  Puts a byte to USART1
 *
 * @param  ch		Byte to send
 * @retval None
 */
void USART1_Put(uint8_t ch)
{
	//unsigned long timerWait = millis();
	USART_SendData(USART1, (uint8_t) ch);
	//Loop until the end of transmission or timeout appears
	while (USART_GetFlagStatus(USART1, USART_FLAG_TC) == RESET)
	{
		//if ((millis() - timerWait) >= USART_TIMEOUT)
		//	break;
	}
}

/**
 * @brief  Gets one byte from USART1
 *
 * @retval None
 */
uint8_t USART1_Get(void)
{
	//unsigned long timerWait = millis();
	//bool timeout = false;
	while (USART_GetFlagStatus(USART1, USART_FLAG_RXNE) == RESET)
	{
			//if ((millis() - timerWait) >= USART_TIMEOUT) {
			//	timeout = true;
			//	break;
			//}
	};
	//if (timeout)
	//	return 0;
	//else
		return (uint8_t) USART_ReceiveData(USART1);
}

/**
 * @brief  Put string to USART1
 *
 * @param  s		String to send
 * @retval None
 */
void USART1_Puts(volatile char *s)
{
	unsigned long timerWait = millis();
	bool timeout = false;
	while (*s)
	{
		// wait until data register is empty
		while (!(USART1->SR & 0x00000040)) {
			/*if ((millis() - timerWait) >= USART_TIMEOUT) {
				timeout = true;
				break;
			}*/
		};
		//if (timeout) {
		//	return false;
		//} else {
			USART_SendData(USART1, *s++);
		//	return true;
		//}
		//*s++;
	}
}

/**
 * @brief  This is the interrupt request handler (IRQ) for ALL USART1 interrupts
 *
 * @retval None
 */
void USART1_IRQHandler(void)
{

// check if the USART1 receive interrupt flag was set
	if (USART_GetITStatus(USART1, USART_IT_RXNE))
	{
		// get the new byte:
		char inChar = (uint8_t) USART_ReceiveData(USART1);
		//USART3_Put(inChar);
		//return;

		// if the new byte is a start text character start the packet receiving
		if (inChar == STX) {
			// add it to the inputString:
			//inputString += inChar;
			//inputString[inputPos] = inChar;
			packetStarted = true;
			packetTimer = millis();
		}

		if (packetStarted) {
			// check if packet timeout reached
			if ((millis() - packetTimer) >= PACKET_TIMEOUT) {
				packetStarted = false;
				packetError = true;
				//return;
			}
			// check for special enquiry command
			if (inChar == ENQ) {
				//delay(15);
				// read the next char to identify replacement value
				while(USART_GetFlagStatus(USART1, USART_FLAG_RXNE) == RESET);//TODO timeout
				inChar = (uint8_t) USART_ReceiveData(USART1);
				if (inChar == DC2) {
					// add a 02 to the inputString:
					//inputString += (char)0x02;
					inputString[inputPos] = (char)0x02;
				} else if (inChar == DC3) {
					// add a 03 to the inputString:
					//inputString += (char)0x03;
					inputString[inputPos] = (char)0x03;
				} else if (inChar == NAK) {
					// add a 05 to the inputString:
					//inputString += (char)0x05;
					inputString[inputPos] = (char)0x05;
				//} else {
				//	inputString += (char)0x05;
				//	inputString += inChar;
				}
				if (inputString[1] == COMMAND_TEXT) inputString[2] = inputString[2] - 1;
			} else if (inChar == ETX) {
					// add it to the inputString:
					//inputString += inChar;
					inputString[inputPos] = inChar;
					packetComplete = true;
					packetType = 0;
					packetStarted = false;
					//inputString = "";
					// if not a enquire nor an end text char add it

					// if the incoming character is a end text character, set a flag
					// so the main loop can do further processing
			} else {
				// add it to the inputString:
				//inputString += inChar;
				inputString[inputPos] = inChar;
			}
		}

		inputPos++;
	}
}

/**
 * @}
 */

/**
 * @}
 */

/**
 * @}
 */
