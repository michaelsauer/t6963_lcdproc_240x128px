/*
 * keys.c
 *
 *  Created on: 07.11.2016
 *      Author: michael
 */

#include "keys.h"
#include "main.h"

/* Key debouncing */
int buttonState_A;             // the current reading from the input pin
int lastButtonState_A = 1;   // the previous reading from the input pin
unsigned long lastDebounceTime_A = 0;  // the last time the output pin was toggled

int buttonState_B;             // the current reading from the input pin
int lastButtonState_B = 1;   // the previous reading from the input pin
unsigned long lastDebounceTime_B = 0;  // the last time the output pin was toggled

int buttonState_C;             // the current reading from the input pin
int lastButtonState_C = 1;   // the previous reading from the input pin
unsigned long lastDebounceTime_C = 0;  // the last time the output pin was toggled

int buttonState_D;             // the current reading from the input pin
int lastButtonState_D = 1;   // the previous reading from the input pin
unsigned long lastDebounceTime_D = 0;  // the last time the output pin was toggled

unsigned long debounceDelay = 50;    // the debounce time; increase if the output flickers

void Keys_Processing()
{
	// read the state of the switch into a local variable:
	int reading_A = GPIO_ReadInputDataBit(GPIOB, GPIO_Pin_0);
	int reading_B = GPIO_ReadInputDataBit(GPIOD, GPIO_Pin_2);
	int reading_C = GPIO_ReadInputDataBit(GPIOC, GPIO_Pin_12);
	int reading_D = GPIO_ReadInputDataBit(GPIOC, GPIO_Pin_13);


	// If the switch changed, due to noise or pressing:
	if (reading_A != lastButtonState_A) {
		// reset the debouncing timer
		lastDebounceTime_A = millis();
	}

	if ((millis() - lastDebounceTime_A) > debounceDelay) {
		// whatever the reading is at, it's been there for longer
		// than the debounce delay, so take it as the actual current state:

		// if the button state has changed:
		if (reading_A != buttonState_A) {
			buttonState_A = reading_A;

			// only toggle the LED if the new button state is HIGH
			if (buttonState_A == 0) {
				USART1_Put((char)COMMAND_KEY);
				USART1_Put((char)KEY_A_BYTE);
				sendTCPKey(KEY_A);
			}
		}
	}

	if ((millis() - lastDebounceTime_B) > debounceDelay) {
		// whatever the reading is at, it's been there for longer
		// than the debounce delay, so take it as the actual current state:

		// if the button state has changed:
		if (reading_B != buttonState_B) {
			buttonState_B = reading_B;

			// only toggle the LED if the new button state is LOW
			if (buttonState_B == 0) {
				USART1_Put((char)COMMAND_KEY);
				USART1_Put((char)KEY_B_BYTE);
				sendTCPKey(KEY_B);
			}
		}
	}

	if ((millis() - lastDebounceTime_C) > debounceDelay) {
		// whatever the reading is at, it's been there for longer
		// than the debounce delay, so take it as the actual current state:

		// if the button state has changed:
		if (reading_C != buttonState_C) {
			buttonState_C = reading_C;

			// only toggle the LED if the new button state is HIGH
			if (buttonState_C == 0) {
				USART1_Put((char)COMMAND_KEY);
				USART1_Put((char)KEY_C_BYTE);
				sendTCPKey(KEY_C);
			}
		}
	}

	if ((millis() - lastDebounceTime_D) > debounceDelay) {
		// whatever the reading is at, it's been there for longer
		// than the debounce delay, so take it as the actual current state:

		// if the button state has changed:
		if (reading_D != buttonState_D) {
			buttonState_D = reading_D;

			// only toggle the LED if the new button state is HIGH
			if (buttonState_D == 0) {
				USART1_Put((char)COMMAND_KEY);
				USART1_Put((char)KEY_D_BYTE);
				sendTCPKey(KEY_D);
			}
		}
	}

	// save the key reading.  Next time through the loop,
	// it'll be the lastButtonState:
	lastButtonState_A = reading_A;
	lastButtonState_B = reading_B;
	lastButtonState_C = reading_C;
	lastButtonState_D = reading_D;
}
