/**
 * @file adc.c
 *
 * @brief This file includes the ADC functions
 *
 * @author Michael Sauer <sauer.uetersen@gmail.com>
 *
 * @date 20.10.2014 - First Version
 */

#include "adc.h"

/** @addtogroup Libraries
 * @{
 */

/** @addtogroup ADC_Library
 * @{
 */

/** @addtogroup ADC_Private_Defines
 * @{
 */
#define ADC1_DR_Address    ((uint32_t)0x4001244C)
/**
 * @}
 */

/** @addtogroup ADC_Private_Variables
 * @{
 */
__IO uint16_t ADCBuffer[5];
/**
 * @}
 */

/** @addtogroup ADC_Private_Functions
 * @{
 */
void p_IO_Init(void)
{
	GPIO_InitTypeDef GPIO_InitStructure;

	/* ADCCLK = PCLK2/4 */
	RCC_ADCCLKConfig(RCC_PCLK2_Div4);

	/* Enable peripheral clocks ------------------------------------------------*/
	/* Enable ADC1, DMA1 and GPIOC clocks */
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_ADC1 | RCC_APB2Periph_GPIOA, ENABLE);
	RCC_AHBPeriphClockCmd(RCC_AHBPeriph_DMA1, ENABLE);

	/* Configure PA.03, PA.04 and PA.05 (ADC Channel3, ADC Channel4 and
	 ADC Channel5) as analog inputs */
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_2 | GPIO_Pin_3 | GPIO_Pin_4 | GPIO_Pin_5;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN_FLOATING;
	GPIO_Init(GPIOA, &GPIO_InitStructure);
}

void p_NVIC_Init(void)
{
	NVIC_InitTypeDef NVIC_InitStructure;

	NVIC_InitStructure.NVIC_IRQChannel = DMA1_Channel1_IRQn;
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0;
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0;
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&NVIC_InitStructure);
}

void p_DMA_Init(void)
{
	DMA_InitTypeDef DMA_InitStructure;
	/* DMA1 channel1 configuration ----------------------------------------------*/
	DMA_DeInit(DMA1_Channel1);
	DMA_InitStructure.DMA_PeripheralBaseAddr = (u32) &ADC1->DR;
	DMA_InitStructure.DMA_MemoryBaseAddr = (u32) &ADCBuffer[0];
	DMA_InitStructure.DMA_DIR = DMA_DIR_PeripheralSRC;
	DMA_InitStructure.DMA_BufferSize = 5;
	DMA_InitStructure.DMA_PeripheralInc = DMA_PeripheralInc_Disable;
	DMA_InitStructure.DMA_MemoryInc = DMA_MemoryInc_Enable;
	DMA_InitStructure.DMA_PeripheralDataSize = DMA_PeripheralDataSize_HalfWord;
	DMA_InitStructure.DMA_MemoryDataSize = DMA_MemoryDataSize_HalfWord;
	DMA_InitStructure.DMA_Mode = DMA_Mode_Circular;
	DMA_InitStructure.DMA_Priority = DMA_Priority_High;
	DMA_InitStructure.DMA_M2M = DMA_M2M_Disable;
	DMA_Init(DMA1_Channel1, &DMA_InitStructure);

	/* Enable DMA1 channel1 */
	DMA_Cmd(DMA1_Channel1, ENABLE);
}

/**
 * @}
 */

/** @addtogroup ADC_Exported_Functions
 * @{
 */

/**
 * @brief  This function initializes the ADC
 * @retval None
 */
void ADC12_Init(void)
{
	ADC_InitTypeDef ADC_InitStructure;

	p_NVIC_Init();
	p_IO_Init();
	p_DMA_Init();

	/*
	 ADC_RegularChannelConfig(ADC1, ADC_Channel_5, 1, ADC_SampleTime_28Cycles5);
	 ADC_RegularChannelConfig(ADC1, ADC_Channel_4, 2, ADC_SampleTime_55Cycles5);
	 ADC_RegularChannelConfig(ADC1, ADC_Channel_3, 3, ADC_SampleTime_28Cycles5);
	 */

	/* ADC1 configuration ------------------------------------------------------*/
	ADC_InitStructure.ADC_Mode = ADC_Mode_Independent;
	ADC_InitStructure.ADC_ScanConvMode = ENABLE;
	ADC_InitStructure.ADC_ContinuousConvMode = ENABLE;
	ADC_InitStructure.ADC_ExternalTrigConv = ADC_ExternalTrigConv_None;
	ADC_InitStructure.ADC_DataAlign = ADC_DataAlign_Right;
	ADC_InitStructure.ADC_NbrOfChannel = 5;
	ADC_Init(ADC1, &ADC_InitStructure);

	ADC_TempSensorVrefintCmd(ENABLE);
	/* ADC1 regular channels configuration */
	ADC_RegularChannelConfig(ADC1, ADC_Channel_5, 1, ADC_SampleTime_239Cycles5);
	ADC_RegularChannelConfig(ADC1, ADC_Channel_4, 2, ADC_SampleTime_239Cycles5);
	ADC_RegularChannelConfig(ADC1, ADC_Channel_4, 3, ADC_SampleTime_239Cycles5);
	ADC_RegularChannelConfig(ADC1, ADC_Channel_TempSensor, 4, ADC_SampleTime_71Cycles5);
	ADC_RegularChannelConfig(ADC1, ADC_Channel_Vrefint, 5, ADC_SampleTime_239Cycles5);

	/* Enable ADC1 */
	ADC_Cmd(ADC1, ENABLE);

	/* Enable ADC1 reset calibaration register */
	ADC_ResetCalibration(ADC1);

	/* Check the end of ADC1 reset calibration register */
	while (ADC_GetResetCalibrationStatus(ADC1))
		;

	/* Start ADC1 calibaration */
	ADC_StartCalibration(ADC1);

	/* Check the end of ADC1 calibration */
	while (ADC_GetCalibrationStatus(ADC1))
		;

	/* Enable ADC1 DMA */
	ADC_DMACmd(ADC1, ENABLE);

	ADC_SoftwareStartConvCmd(ADC1, ENABLE);

	/* Enable ADC1 DMA */
	ADC_DMACmd(ADC1, ENABLE);

	//DMA_ITConfig(DMA1_Channel1, DMA_IT_TC, ENABLE);
}

/**
 * @brief  This function returns value of the selected filter from @ref ADC_FILTER
 * @param	filter	The Filter to read out
 * @retval ADC Value of Filter
 */
uint16_t ADC_GetValue(ADC_FILTER filter)
{
	//while( ADC_GetFlagStatus( ADC1, ADC_FLAG_EOC ) == RESET );
	return ADCBuffer[filter];
}

/*! @brief Maps a number to a new value space
 *
 *  @param x        Value to be converted
 *  @param in_min   Smallest input Value
 *  @param in_max   Biggest input Value
 *  @param out_min  Smallest output Value
 *  @param out_max  Biggest output Value
 *
 *  @result Mapped value
 */
long ADC_Map(long x, long in_min, long in_max, long out_min, long out_max)
{
	return (x - in_min) * (out_max - out_min) / (in_max - in_min) + out_min;
}

/**
 * @}
 */

/**
 * @}
 */

/**
 * @}
 */
