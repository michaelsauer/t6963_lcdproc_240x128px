/*
 * t6963.h
 *
 *  Created on: 21.10.2016
 *      Author: michael
 */

#ifndef LIB_T6963_H_
#define LIB_T6963_H_

#include "stm32f10x_gpio.h"
#include "stm32f10x_rcc.h"
#include "general.h"
#include "font.h"

/*
 * Liste aller Pins für das Display
 * (keine Nummer doppelt und von 0 beginnend)
 */
typedef enum
{
  TLCD_WR = 0,    // Write-Pin
  TLCD_RD  = 1,   // Read-Pin
  TLCD_CE = 2,    // CE-Pin
  TLCD_CD = 3,    // C/D-Pin
  TLCD_RST = 4,   // Reset-Pin
  TLCD_D0 = 5,    // D0-Pin
  TLCD_D1 = 6,    // D1-Pin
  TLCD_D2 = 7,    // D2-Pin
  TLCD_D3 = 8,    // D3-Pin
  TLCD_D4 = 9,    // D4-Pin
  TLCD_D5 = 10,   // D5-Pin
  TLCD_D6 = 11,   // D6-Pin
  TLCD_D7 = 12,   // D7-Pin
  TLCD_REV= 13,   // Reverse-Pin
  TLCD_BL= 14,   // Backlight-Pin
}TLCD_NAME_t;

#define  TLCD_ANZ   15 // Anzahl von TLCD_NAME_t

#define T6963_PIXEL_VERTICAL 240
#define T6963_PIXEL_HORIZONTAL 128
#define T6963_MEMORY_SIZE 32
#define T6963_FONT_WIDTH 6

/* Struktur eines Pins */
typedef struct {
  TLCD_NAME_t TLCD_NAME;	// Name
  GPIO_TypeDef* TLCD_PORT; 	// Port
  const uint16_t TLCD_PIN; 	// Pin
  const uint8_t TLCD_PINNUM;// Pin Number
  const uint32_t TLCD_CLK; 	// Clock
  BitAction TLCD_INIT;     	// Init
}LCD_t;

/*
uint8_t GLCD_NUMBER_OF_LINES = T6963_PIXEL_VERTICAL;
uint8_t GLCD_PIXELS_PER_LINE = T6963_PIXEL_HORIZONTAL;
#define _GA (T6963_PIXEL_HORIZONTAL / T6963_FONT_WIDTH);	//Supercedes GLCD_GRAPHIC_AREA
#define _TA (T6963_PIXEL_HORIZONTAL / T6963_FONT_WIDTH);	//Supercedes GLCD_TEXT_AREA
uint8_t _sizeMem = T6963_MEMORY_SIZE; 								//size of attached memory in kb.
uint16_t sizeGA = _GA * T6963_PIXEL_VERTICAL;					//Supercedes GLCD_GRAPHIC_SIZE
uint16_t sizeTA = _TA * T6963_PIXEL_VERTICAL / 8;	        	//Supercedes GLCD_TEXT_SIZE
*/

#define T6963_OFFSET_REGISTER  0x03 //0x01//0x02//0x1C
#define T6963_EXTERNAL_CG_HOME 0x1C00 //0x0C00//0x1400//0xE400 //(GLCD_OFFSET_REGISTER << 11)

/* T6963C Commands */
#define T6963_SET_CURSOR_POINTER	0x21
#define T6963_SET_OFFSET_REGISTER	0x22
#define T6963_SET_ADDRESS_POINTER	0x24

#define T6963_SET_TEXT_HOME_ADDRESS	0x40
#define T6963_SET_TEXT_AREA		0x41
#define T6963_SET_GRAPHIC_HOME_ADDRESS	0x42
#define T6963_SET_GRAPHIC_AREA		0x43

#define T6963_MODE_SET                  0x80

#define T6963_SET_DATA_AUTO_WRITE	0xB0
#define T6963_SET_DATA_AUTO_READ	0xB1
#define T6963_AUTO_RESET		0xB2

#define T6963_DATA_WRITE_AND_INCREMENT	  0xC0
#define T6963_DATA_READ_AND_INCREMENT	  0xC1
#define T6963_DATA_WRITE_AND_DECREMENT	  0xC2
#define T6963_DATA_READ_AND_DECREMENT	  0xC3
#define T6963_DATA_WRITE_AND_NONVARIALBE  0xC4
#define T6963_DATA_READ_AND_NONVARIABLE	  0xC5

#define T6963_CURSOR_PATTERN_SELECT 0xA0 //cursor patter select command prefix or with desired size-1.

#define T6963_DISPLAY_MODE 0x90

#define T6963_SCREEN_PEEK		0xE0
#define T6963_SCREEN_COPY		0xE8

void T6963_Init(void);
void T6963_Reset(void);
void T6963_WriteCommand(uint8_t command);
void T6963_SetAddressPointer(unsigned int address);
uint8_t T6963_ReadData(void);
void T6963_ClearText();
void T6963_ClearCG();
void T6963_ClearGraphic();
char T6963_SetMode(char _mode, char _CG);
void T6963_Reverse(bool state);
char T6963_SetDispMode(bool _text, bool _graphics, bool _cursor, bool _blink);
void T6963_WriteData(uint8_t data);
void T6963_WriteDisplayData(uint8_t x);
void T6963_WriteDataNon(uint8_t x);
void T6963_WriteDataDec(uint8_t x);
void T6963_TextGoTo(unsigned char x, unsigned char y);
void T6963_WriteString(char * string);
void T6963_WriteChar(char charCode);
void T6963_WriteCharDec(char charCode);
void T6963_WriteCGChar(unsigned char c);
void T6963_CreateLine(int x0, int y0, int x1, int y1, char color);
void T6963_WritePixel(uint8_t x, uint8_t y, uint8_t color);
uint8_t T6963_SetPixel(uint8_t x, uint8_t y);
uint8_t T6963_ClearPixel(uint8_t x, uint8_t y);
void T6963_CreateCircle(int cx, int cy, int radius, uint8_t color);
void T6963_Plot8Points(int cx, int cy, int x, int y, uint8_t color);
void T6963_Plot4Points(int cx, int cy, int x, int y, uint8_t color);
void T6963_ClearDispMode();
uint8_t T6963_SetTextAttrMode(char _mode);
void T6963_Drawrectbyte(unsigned int x, unsigned int y, unsigned int HEIGHT, unsigned int BYTEWIDTH, uint8_t pattern);
void T6963_FillRect(int x1, int y1, int x2, int y2, uint8_t color);
void T6963_Rectangle(unsigned char x, unsigned char y, unsigned char b, unsigned char a, uint8_t color);
void T6963_DrawPic(unsigned int x, unsigned int y, unsigned char * PicName, unsigned int HEIGHT, unsigned int BYTEWIDTH);
void T6963_DrawAnim(unsigned int x, unsigned int y, unsigned char** string_table, int l, unsigned int HEIGHT, unsigned int BYTEWIDTH);
void T6963_GraphicGoTo(uint8_t x, uint8_t y);
void T6963_Glcd_print1_P(unsigned char x,unsigned char y, const char *in, const struct FONT_DEF *strcut1, unsigned char invers);
void T6963_Glcd_print2_P(unsigned char x,unsigned char y, const char *in, const struct FONT_DEF *strcut1, unsigned char invers);
unsigned int T6963_DefineCharacter(uint8_t charCode, unsigned char * defChar);
uint8_t T6963_SetCursorPattern(uint8_t _b);
void T6963_SetCursorPointer(uint8_t _col, uint8_t _row);
void T6963_Backlight(bool state);

#endif /* LIB_T6963_H_ */
