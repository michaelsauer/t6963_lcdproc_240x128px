/*
 * encoder.c
 *
 *  Created on: 24.10.2016
 *      Author: michael
 */
#include "encoder.h"
#include "main.h"
#include "keys.h"

EXTI_InitTypeDef   EXTI_InitStructure;
GPIO_InitTypeDef   GPIO_InitStructure;
NVIC_InitTypeDef   NVIC_InitStructure;

//int8_t add_subt[16] = { 0, 1 ,-1 , 0, 0, 1, -1, 0, 0, 1, -1, 0, 0, 1, -1, 0 };
int8_t add_subt[16] = {0,0,-1,0,0,0,0,1,1,0,0,0,0,-1,0,0}; //Halbe auflösung
//int8_t add_subt[16] = {0,1,-1,0,-1,0,0,1,1,0,0,-1,0,-1,1,0}; //volle auflösung
volatile uint8_t idx;
volatile int32_t pos_count;

/* Rotary Encoder Debouncing */
unsigned long encoderDebounceTime = 0; // the last time the rotary encoder was toggled
unsigned long encoderDebounceDelay = 50;    // the debounce time; increase if the output flickers
uint32_t old_pos = 0;
uint32_t new_pos = 0;

void Encoder_Init_IO(void);

ENCODER_t ENCODER[] =
{
	// Name,     PORT,  PIN,      PINNUM,  CLOCK,            Init,      EXTI AFIO CLOCK,     PINMODE,               EXTI Pin Source, EXTI Port Source,     EXTI Line, EXTI IRQn,   EXTI MODE
	{ ENC_BTN,   GPIOB, GPIO_Pin_0, 0, RCC_APB2Periph_GPIOB, Bit_RESET, RCC_APB2Periph_AFIO, GPIO_Mode_IPU,         GPIO_PinSource0, GPIO_PortSourceGPIOB, EXTI_Line0, EXTI0_IRQn, EXTI_Trigger_Falling },
	{ ENC_LEFT,  GPIOB, GPIO_Pin_1, 1, RCC_APB2Periph_GPIOB, Bit_RESET, RCC_APB2Periph_AFIO, GPIO_Mode_IN_FLOATING, GPIO_PinSource1, GPIO_PortSourceGPIOB, EXTI_Line1, EXTI1_IRQn, EXTI_Trigger_Rising_Falling },
	{ ENC_RIGHT, GPIOB, GPIO_Pin_2, 2, RCC_APB2Periph_GPIOB, Bit_RESET, RCC_APB2Periph_AFIO, GPIO_Mode_IN_FLOATING, GPIO_PinSource2, GPIO_PortSourceGPIOB, EXTI_Line2, EXTI2_IRQn, EXTI_Trigger_Rising_Falling },
};

Encoder_internal_state_t encoder;

/* Initializes the Rotary Encoder */
void Encoder_Init(void)
{
	// init aller IO-Pins
	Encoder_Init_IO();

	uint8_t s = 0;
	if (ENCODER[ENC_LEFT].ENC_PORT->IDR & (1 << ENCODER[ENC_LEFT].ENC_PINNUM)) s |= 2;
	if (ENCODER[ENC_RIGHT].ENC_PORT->IDR & (1 << ENCODER[ENC_RIGHT].ENC_PINNUM)) s |= 1;
	encoder.state = s;

	idx = (s << 2) + s;
}

uint32_t Encoder_GetPos(void)
{
	return pos_count;
}

void Encoder_Processing(void)
{
	// get actual rotary encoder position
	uint32_t pos = Encoder_GetPos();

	if (pos != old_pos) {
		//xprintf("%d -> %d\n", pos, old_pos);
		encoderDebounceTime = millis();
	}
	if ((millis() - encoderDebounceTime) > (encoderDebounceDelay*2)) {
		//xprintf("%d\n", diff);
		if (pos != new_pos) {
			int8_t diff = pos - new_pos;
			//xprintf("%d\n", diff);
			new_pos = pos;
			if (diff > 0) { // Links rum
				USART1_Put((char)COMMAND_KEY);
				USART1_Put((char)KEY_E_BYTE);
				sendTCPKey(KEY_E);
				//xprintf("+1 %d\r\n", (int32_t)pos/2);
			} else if (diff < 0) { // Rechts rum
				//xprintf("-1 %d\r\n", (int32_t)pos/2);
				USART1_Put((char)COMMAND_KEY);
				USART1_Put((char)KEY_F_BYTE);
				sendTCPKey(KEY_F);
			}
		}

	}
	old_pos = pos;
}

//--------------------------------------------------------------
// interne Funktion
// init aller IO-Pins
//--------------------------------------------------------------
void Encoder_Init_IO(void)
{
	GPIO_InitTypeDef GPIO_InitStructure;

	/*
	ENC_NAME_t enc_pin;
	for (enc_pin = 0; enc_pin < ENC_ANZ; enc_pin++)
	{
		// Clock Enable
		RCC_APB2PeriphClockCmd(ENCODER[enc_pin].ENC_CLK, ENABLE);

		// Config als Digital-Eingang
		GPIO_InitStructure.GPIO_Pin = ENCODER[enc_pin].ENC_PIN;
		GPIO_InitStructure.GPIO_Mode = ENCODER[enc_pin].ENC_MODE;
		GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
		GPIO_Init(ENCODER[enc_pin].ENC_PORT, &GPIO_InitStructure);

		// Default Wert einstellen
		if (ENCODER[enc_pin].ENC_INIT == Bit_RESET)
			GPIO_ResetBits(ENCODER[enc_pin].ENC_PORT, ENCODER[enc_pin].ENC_PIN);
		else
			GPIO_SetBits(ENCODER[enc_pin].ENC_PORT, ENCODER[enc_pin].ENC_PIN);


		// Enable AFIO clock
		RCC_APB2PeriphClockCmd(ENCODER[enc_pin].ENC_AFIO, ENABLE);

		GPIO_EXTILineConfig(ENCODER[enc_pin].ENC_PORTSRC, ENCODER[enc_pin].ENC_PINSRC);

		// Configure EXTI line
		EXTI_InitStructure.EXTI_Line = ENCODER[enc_pin].ENC_ILINE;
		EXTI_InitStructure.EXTI_Mode = EXTI_Mode_Interrupt;
		EXTI_InitStructure.EXTI_Trigger = ENCODER[enc_pin].ENC_IRQMODE;
		EXTI_InitStructure.EXTI_LineCmd = ENABLE;
		EXTI_Init(&EXTI_InitStructure);

		// Enable and set EXTI Interrupt to the lowest priority
		NVIC_InitStructure.NVIC_IRQChannel = ENCODER[enc_pin].ENC_IRQN;
		NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0x0F;
		NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0x0F;
		NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;

		NVIC_Init(&NVIC_InitStructure);

	}
	*/
		RCC_APB2PeriphClockCmd(RCC_APB2Periph_AFIO, ENABLE);
		GPIO_EXTILineConfig(GPIO_PortSourceGPIOB, GPIO_PinSource0);
		EXTI_InitStructure.EXTI_Line = EXTI_Line0;
		EXTI_InitStructure.EXTI_Mode = EXTI_Mode_Interrupt;
		EXTI_InitStructure.EXTI_Trigger = EXTI_Trigger_Falling;
		EXTI_InitStructure.EXTI_LineCmd = ENABLE;
		EXTI_Init(&EXTI_InitStructure);
		NVIC_InitStructure.NVIC_IRQChannel = EXTI0_IRQn;
		NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0x0F;
		NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0x0F;
		NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
		NVIC_Init(&NVIC_InitStructure);

		GPIO_EXTILineConfig(GPIO_PortSourceGPIOB, GPIO_PinSource1);
		EXTI_InitStructure.EXTI_Line = EXTI_Line1;
		EXTI_InitStructure.EXTI_Mode = EXTI_Mode_Interrupt;
		EXTI_InitStructure.EXTI_Trigger = EXTI_Trigger_Rising_Falling;
		EXTI_InitStructure.EXTI_LineCmd = ENABLE;
		EXTI_Init(&EXTI_InitStructure);
		NVIC_InitStructure.NVIC_IRQChannel = EXTI1_IRQn;
		NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0x0F;
		NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0x0F;
		NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
		NVIC_Init(&NVIC_InitStructure);

		GPIO_EXTILineConfig(GPIO_PortSourceGPIOB, GPIO_PinSource2);
		EXTI_InitStructure.EXTI_Line = EXTI_Line2;
		EXTI_InitStructure.EXTI_Mode = EXTI_Mode_Interrupt;
		EXTI_InitStructure.EXTI_Trigger = EXTI_Trigger_Rising_Falling;
		EXTI_InitStructure.EXTI_LineCmd = ENABLE;
		EXTI_Init(&EXTI_InitStructure);
		NVIC_InitStructure.NVIC_IRQChannel = EXTI2_IRQn;
		NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0x0F;
		NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0x0F;
		NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
		NVIC_Init(&NVIC_InitStructure);
}

void EXTI0_IRQHandler(void)
{
		if(EXTI_GetITStatus(EXTI_Line0) != RESET)
		{
			//xprintf("\x00");
			/* Clear the  EXTI line 0 pending bit */
			EXTI_ClearITPendingBit(EXTI_Line0);
		}


}

void EXTI1_IRQHandler(void)
{
	//GPIO_SetBits(GPIOC, GPIO_Pin_4);
		if(EXTI_GetITStatus(EXTI_Line1) != RESET)
		{
			uint8_t s = 0;
			if (ENCODER[ENC_LEFT].ENC_PORT->IDR & (1 << ENCODER[ENC_LEFT].ENC_PINNUM)) s |= 2;
			if (ENCODER[ENC_RIGHT].ENC_PORT->IDR & (1 << ENCODER[ENC_RIGHT].ENC_PINNUM)) s |= 1;
			//if (GPIOB->IDR & (1 << ENCODER[ENC_LEFT].ENC_PINNUM)) s |= 2;
			//if (GPIOB->IDR & (1 << ENCODER[ENC_RIGHT].ENC_PINNUM)) s |= 1;

			idx = ((idx << 2) & 0x0F) + s;
			pos_count = pos_count + add_subt[idx];
			//xprintf("%04b\n", idx);
			//xprintf("\x01");
			EXTI_ClearITPendingBit(EXTI_Line1);
		}
}

void EXTI2_IRQHandler(void)
{
		if(EXTI_GetITStatus(EXTI_Line2) != RESET)
		{
			uint8_t s = 0;
			if (ENCODER[ENC_LEFT].ENC_PORT->IDR & (1 << ENCODER[ENC_LEFT].ENC_PINNUM)) s |= 2;
			if (ENCODER[ENC_RIGHT].ENC_PORT->IDR & (1 << ENCODER[ENC_RIGHT].ENC_PINNUM)) s |= 1;

			//if (GPIOB->IDR & (1 << ENCODER[ENC_LEFT].ENC_PINNUM)) s |= 2;
			//if (GPIOB->IDR & (1 << ENCODER[ENC_RIGHT].ENC_PINNUM)) s |= 1;

			idx = ((idx << 2) & 0x0F) + s;
			pos_count = pos_count + add_subt[idx];
			//xprintf("%04b\n", idx);
			//xprintf("%d - %d - %04b\n", pos_count, add_subt[idx], idx);
			/* Clear the  EXTI line 0 pending bit */
			EXTI_ClearITPendingBit(EXTI_Line2);
		}
}

/**
  * @brief  This function handles External lines 9 to 5 interrupt request.
  * @param  None
  * @retval None
  */
void EXTI9_5_IRQHandler(void)
{
	ENC_NAME_t enc_pin;

	for (enc_pin = 0; enc_pin < ENC_ANZ; enc_pin++)
	{
		if(EXTI_GetITStatus(ENCODER[enc_pin].ENC_ILINE) != RESET)
		{
			//xprintf("\x30");
			/* Clear the  EXTI line 0 pending bit */
			EXTI_ClearITPendingBit(ENCODER[enc_pin].ENC_ILINE);
		}
	}
}

