/**
 * @file uart3.h
 *
 * @brief This file includes the UART3 functions
 *
 * @author Michael Sauer <sauer.uetersen@gmail.com>
 *
 * @date 14.09.2014 - erste Version
 *
 */

//--------------------------------------------------------------
#ifndef _UART3_H
#define _UART3_H

//--------------------------------------------------------------
// Includes
//--------------------------------------------------------------
#include <string.h>
#include <stdlib.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <stddef.h>
#include <errno.h>
#include <sys/stat.h>
#include <sys/times.h>
#include <sys/unistd.h>
#include "stm32f10x.h"
#include "general.h"

/** @addtogroup Libraries
  * @{
  */

/** @addtogroup UART3_Library
  * @{
  */

/** @addtogroup UART3_Exported_Functions_Prototypes
  * @{
  */

void USART3_Init(uint32_t baudrate);
void USART3_Put(uint8_t ch);
uint8_t USART3_Get(void);
void USART3_Puts(volatile char *s);
void USART3_Send(uint8_t* DataArray, uint16_t count);

/**
  * @}
  */

/**
  * @}
  */

/**
  * @}
  */

//--------------------------------------------------------------
#endif // __UART3_H
