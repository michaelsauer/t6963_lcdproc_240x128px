/*
 * encoder.h
 *
 *  Created on: 24.10.2016
 *      Author: michael
 */

#ifndef LIB_ENCODER_H_
#define LIB_ENCODER_H_

#include "stm32f10x_gpio.h"
#include "stm32f10x_rcc.h"
#include "general.h"

/*
 * Liste aller Pins für den Encoder
 * (keine Nummer doppelt und von 0 beginnend)
 */
typedef enum
{
  ENC_BTN = 0,    // Button-Pin
  ENC_LEFT  = 1,   // Left Encoder-Pin
  ENC_RIGHT = 2,    // Right Encoder-Pin
}ENC_NAME_t;
#define  ENC_ANZ   3 // Anzahl von ENC_NAME_t

/* Struktur eines Pins */
typedef struct {
  ENC_NAME_t ENC_NAME;	// Name
  GPIO_TypeDef* ENC_PORT; 	// Port
  const uint16_t ENC_PIN; 	// Pin
  const uint8_t ENC_PINNUM;// Pin Number
  const uint32_t ENC_CLK; 	// Clock
  GPIOMode_TypeDef ENC_MODE; // Pinmode
  BitAction ENC_INIT;     	// Init
  const uint32_t ENC_AFIO; // EXTI AFIO Clock
  const uint8_t ENC_PINSRC; // EXTI Pin Source
  const uint8_t ENC_PORTSRC; // EXTI Port Source
  const uint32_t ENC_ILINE; // EXTI Line Source
  IRQn_Type ENC_IRQN; // EXTI IRQn
  EXTITrigger_TypeDef ENC_IRQMODE; // EXTI IRQ Mode

}ENCODER_t;

// All the data needed by interrupts is consolidated into this ugly struct
// to facilitate assembly language optimizing of the speed critical update.
// The assembly code uses auto-incrementing addressing modes, so the struct
// must remain in exactly this order.
typedef struct {
	uint8_t                state;
	int32_t                position;
} Encoder_internal_state_t;

void Encoder_Init(void);
uint32_t Encoder_GetPos(void);
void Encoder_Processing(void);

#endif /* LIB_ENCODER_H_ */
