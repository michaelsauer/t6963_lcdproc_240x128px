/**
 * @file uart1.h
 *
 * @brief This file includes the UART1 functions
 *	      It handles ML Protocol CRC Check and
 *	      general parsing of the MagicLight Protocol.
 *
 * @author Michael Sauer <sauer.uetersen@gmail.com>
 *
 * @date 14.09.2014 - erste Version
 *
 * @note Packet Format:
 * STX | LEN | ADR | DATA | BCC | 0x0D (\\r) | 0x0A (\\n)
 */

//--------------------------------------------------------------
#ifndef _UART1_H
#define _UART1_H

//--------------------------------------------------------------
// Includes
//--------------------------------------------------------------
#include <string.h>
#include <stdlib.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <stddef.h>
#include <errno.h>
#include <sys/stat.h>
#include <sys/times.h>
#include <sys/unistd.h>
#include "stm32f10x.h"
#include "general.h"

#define PACKET_TIMEOUT 1500
#define USART_TIMEOUT 1000

/** @addtogroup Libraries
  * @{
  */

/** @addtogroup UART1_Library
  * @{
  */

/** @addtogroup UART1_Exported_Functions_Prototypes
  * @{
  */

void USART1_Init(uint32_t baudrate);
void USART1_Put(uint8_t ch);
uint8_t USART1_Get(void);
void USART1_Puts(volatile char *s);
void Log(uint8_t level, char* msg);

/**
  * @}
  */

/**
  * @}
  */

/**
  * @}
  */

//--------------------------------------------------------------
#endif // __UART1_H
