# README #

This Source is for special version of a ULA-200 (originally developed by ELV) which works through serial RS-232 and emulates the original 
ULA-200 interface, beside this connection type is a connection through the TCP/IP over Wifi with an ESP8266 ESP-01 Wifi Module from Espressif.
Display itself got a modded backlight, that would replaced the original EL-Backlight with a LED strip with 18 WS2812B LEDS, so the display 
could be illuminated in every color and brightness.

The Display can be used without modifications with serdisplib and LCDproc, but i made 2 special LCDproc drivers to use more functionality like
the original ULA-200 can do, like contrast and brightness control, positionable better bigger numbers, ...

### What Hardware is used? ###

* STM32F103RB as Microcontroller on Nucleo F103 Board
* ESP-01 ESP8266 Wifi Module
* Graphic LCD Display (240x128px) with T6963C Controller
* WS2812B LED Strip (18 LEDS)
* Selfmade Mainboard
* Selfmade Power Supply for 12V/5V/-24V

![20161102_115944.jpg](https://bitbucket.org/repo/zrxkX9/images/3365053515-20161102_115944.jpg)
![20161106_175350.jpg](https://bitbucket.org/repo/zrxkX9/images/684802791-20161106_175350.jpg)
![20161102_221722.jpg](https://bitbucket.org/repo/zrxkX9/images/4044235232-20161102_221722.jpg)

[Link to the Image Gallery](https://goo.gl/photos/n5KZ2xj2SU4pL9fc9)