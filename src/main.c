/*
 * @version 0.4.1359
 */

/*
 * TODO: Include Defines for the buttons
 * TODO: Move Packet parsing out of main.c
 * TODO: Repair WS2812 controlling and add contrast/brightness functions
 */

/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "cchars.h" // t6963 custom char definitions

/* Private typedef -----------------------------------------------------------*/
typedef enum Symbol {
	SYMBOL_BLOCK_FILLED,
	SYMBOL_HEART_FILLED,
	SYMBOL_HEART_OPEN
} Symbol;/* Private define ------------------------------------------------------------*/
/* Used Display size in chars*/
#define LCD_WIDTH 40
#define LCD_HEIGHT 16

/* Used in Background Rainbow Loop */
#define RAINBOW_DELAY 60

/* Private macro -------------------------------------------------------------*/
#define sendAck() USART1_Put(ACK);
#define sendNak() USART1_Put(NAK)
/* Private variables ---------------------------------------------------------*/
ErrorStatus HSEStartUpStatus;

/* Packet processing */
char * inputString;         // a string to hold incoming data
int inputPos = 0;			// integer to hold data position
__IO boolean packetComplete = false;  // whether the packet is complete
__IO boolean packetStarted = false;  // whether the packet receiving is started
__IO boolean packetError = false; // whether the packet got an error
__IO int packetType = 0; // packet type 0 for serial 1 for tcp
unsigned long packetTimer = 0; // integer to hold the time the packet is processed to make timeout

/* Display variables */
int current_x = 0;
int current_y = 0;
bool clearDisplay = false;

/* Backlight Variables */
uint32_t color = 0x0;
uint16_t h = 0, s = 100, v = 5;
uint32_t timerRainbow = 0;

/* Version Variables */
static uint8_t ver_maj = VER_MAJOR;
static uint8_t ver_min = VER_MINOR;
static uint16_t ver_build = VER_BUILD;

unsigned long tcp_packetTimer;
bool tcp_packetTimeout;

ESP8266_t ESP8266;
ESP8266_Connection_t* ESP_Conn;

/* Private function prototypes -----------------------------------------------*/
void IO_Init(void);
void RCC_Configuration(void);
void NVIC_Configuration(void);
int parsePacket(ESP8266_t* ESP8266, ESP8266_Connection_t* Connection, char *packet);
bool packetTimeout(void);
void printClocks(void);
void SysTick_Handler(void);
int sendTCPAck(ESP8266_t* ESP8266, ESP8266_Connection_t* Connection);
int sendTCPNak(ESP8266_t* ESP8266, ESP8266_Connection_t* Connection);
void ESP8266_Callback_WifiGotIP(ESP8266_t* ESP8266);
void ESP8266_Callback_ServerConnectionDataReceived(ESP8266_t* ESP8266, ESP8266_Connection_t* Connection, uint8_t* Buffer, uint8_t Length);

int main(void)
{
	int i;
	TIM_TimeBaseInitTypeDef TIM_TimeBase_InitStructure;
	NVIC_InitTypeDef NVIC_InitStructure;

	SystemInit();

	/* Check if the system has resumed from IWDG reset */
	if (RCC_GetFlagStatus(RCC_FLAG_IWDGRST) != RESET)
	{
		/* IWDGRST flag set */
		clearDisplay = true;
		/* Clear reset flags */
		RCC_ClearFlag();
	}

	/* System Clock init */
	RCC_Configuration();

	/* NVIC Configuration init */
	NVIC_Configuration();

	/* SysTick init */
	SysTick_Config(SystemCoreClock / 1000);

	/* GPIO init */
	IO_Init();

	/* USART1 init */
	USART1_Init(UART1_BAUDRATE);
	USART3_Init(UART3_BAUDRATE);

	Delay(600000);
	//USART3_Puts("io!\r\n");
	USART1_Puts("io!\r\n");

	// Init ESP module
	while (ESP8266_Init(&ESP8266, 57600) != ESP_OK) {
		USART1_Puts("Problems with initializing module!\r\n");
	}

	// Set mode to STA+AP
	while (ESP8266_SetMode(&ESP8266, ESP8266_Mode_STA) != ESP_OK);

	// Enable server on port 80
	while (ESP8266_ServerEnable(&ESP8266, 80) != ESP_OK);

	// Module is connected OK
	USART1_Puts("Initialization finished!\r\n");

	/* Init WS2812 LCD Backlight (rgb_lib.c) */
	init_stripe();

	/* Rotary Encoder init */
	Encoder_Init();

	/* Data Watch init */
	DWT_Init();

#ifdef WATCHDOG_ENABLED
	IWDG_WriteAccessCmd(IWDG_WriteAccess_Enable);
	IWDG_SetPrescaler(IWDG_Prescaler_32);
	IWDG_SetReload(1000);
	IWDG_ReloadCounter();
	IWDG_Enable();
#endif

	// reserve memory for input packet string
	inputString = (char*)malloc(256*sizeof(char));
	memset(inputString, 0x00, 256);

	T6963_Init();

	T6963_DefineChars();

	T6963_ClearText();
	T6963_ClearGraphic();
	T6963_TextGoTo(0, 0);

	//T6963_Backlight(1);

	if (clearDisplay) {
		clearDisplay = false;
		sendNak();
	} else {
		T6963_WriteString("ULA-200 T6963c LCDproc Driver by ninharp");
		T6963_TextGoTo(31, 15);
		char buf[32];
		sprintf(buf, "v%d.%d.%d", ver_maj, ver_min, ver_build);
		T6963_WriteString(buf);
		T6963_TextGoTo(0, 0);
	}

	/*
	while(1) {
			color = hsv_to_rgb(h, s, v);
			set_stripe(stripe, color);
			//set_led(stripe, 5, color);
			//set_led(stripe, 1, color);
			refresh_stripe(stripe, NR_OF_LEDS);
			// constant color change
			h ++ ;
			h %= 360;

			for (i = 0; i < 150000; i++) {
			};
		}
	 */

	timerRainbow = millis();
	uint8_t currLed = 0;

	T6963_Reverse(1);

	/* Set Background Color */
	color = hsv_to_rgb(1, 0, 250);
	set_stripe(stripe, color);
	refresh_stripe(stripe, NR_OF_LEDS);

	ESP_Conn = NULL;

	/* Main Loop */
	while (1)
	{
		/* Update ESP module */
		ESP8266_Update(&ESP8266);

		/* Background Rainbow Loop */
		/*
		if ((millis() - timerRainbow) >= RAINBOW_DELAY) {
			color = hsv_to_rgb(h, s, v);
			set_stripe(stripe, color);
			//set_led(stripe, currLed, color);
			//set_led(stripe, 1, color);
			refresh_stripe(stripe, NR_OF_LEDS);
			// constant color change
			//h ++ ;
			h += 1;
			h %= 360;

			if (currLed >= (NR_OF_LEDS-1)) {
				currLed = 0;
			} else {
				currLed ++;
			}

			timerRainbow = millis();
		}
		*/

		/* ULA200 Protocol Parsing */
		// check if packet complete flag is set and process if so
		if (packetComplete && !packetError) {
			/*for (int i = 0; i < strlen(inputString); i++) {
				printf("%d 0x%02X\r\n", i, inputString[i]);
			}*/
			//printf("Packet=%s\r\n", inputString);
			// parse incoming Packet
			if (parsePacket(&ESP8266, ESP_Conn, inputString) == 1) {
				// if parsing was successful send an ack response
				if (packetType == 0) {
					sendAck();
				} else {
					sendTCPAck(&ESP8266, ESP_Conn);
				}
			} else {
				// if parsing fails send a nak response
				if (packetType == 0) {
					sendNak();
				} else {
					sendTCPNak(&ESP8266, ESP_Conn);
				}
			}

			// clear the string:
			//inputString = "";
			memset(inputString, 0x00, 256);
			inputPos = 0;

			// reset Packet flags
			packetStarted = false;
			packetComplete = false;
		} else if (packetError) {
			DWT_Delay_ms(100);
			sendNak();
			packetError = false;
			GPIO_SetBits(GPIOC, GPIO_Pin_4);
		}

		/* Key Buttons Debouncing */
		Keys_Processing();

	  	/* Rotary Encoder Evaluation and Debouncing */
	  	Encoder_Processing();

#ifdef WATCHDOG_ENABLED
	  	/* Reload IWDG counter */
		IWDG_ReloadCounter();
#endif
	}
}

/* Private functions ---------------------------------------------------------*/

/*******************************************************************************
 * Function Name  : IO_Init
 * Description    : Configures the Input/Output Ports.
 * Input          : None
 * Output         : None
 * Return         : None
 *******************************************************************************/
void IO_Init(void)
{
	GPIO_InitTypeDef GPIO_InitStructure;

	/* Init User Button on Nucleo Board PC13 */
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOC, ENABLE);
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_13;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AIN | GPIO_Mode_IPU;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_Init(GPIOC, &GPIO_InitStructure);

	/* Encoder PB1+PB2 + Button PB0 */
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOB, ENABLE);
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_0;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AIN | GPIO_Mode_IPU;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_Init(GPIOB, &GPIO_InitStructure);

	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOB, ENABLE);
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_1 | GPIO_Pin_2;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN_FLOATING;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_Init(GPIOB, &GPIO_InitStructure);

	/* Keys Up + Down PC12, PD2 */
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOC, ENABLE);
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_12;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AIN | GPIO_Mode_IPU;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_Init(GPIOC, &GPIO_InitStructure);

	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOD, ENABLE);
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_2;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AIN | GPIO_Mode_IPU;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_Init(GPIOD, &GPIO_InitStructure);

	/* Debug LED PC4 */
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOC, ENABLE);
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_4 | GPIO_Pin_5;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_Init(GPIOC, &GPIO_InitStructure);
	GPIO_ResetBits(GPIOC, GPIO_Pin_4);
	GPIO_SetBits(GPIOC, GPIO_Pin_5);

	/* LED Blue + Yellow PC8+PC9 */
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOC, ENABLE);
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_8 | GPIO_Pin_9;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_Init(GPIOC, &GPIO_InitStructure);
	GPIO_ResetBits(GPIOC, GPIO_Pin_8);
	GPIO_ResetBits(GPIOC, GPIO_Pin_9);
}

/*******************************************************************************
 * Function Name  : RCC_Configuration
 * Description    : Configures the different system clocks.
 * Input          : None
 * Output         : None
 * Return         : None
 *******************************************************************************/
void RCC_Configuration(void)
{
	// RCC system reset(for debug purpose)
	RCC_DeInit();

	// Enable HSE
	RCC_HSEConfig(RCC_HSE_ON);

	// Wait till HSE is ready
	HSEStartUpStatus = RCC_WaitForHSEStartUp();

	if (HSEStartUpStatus == SUCCESS)
	{
		// HCLK = SYSCLK
		RCC_HCLKConfig(RCC_SYSCLK_Div1);

		// PCLK2 = HCLK
		RCC_PCLK2Config(RCC_HCLK_Div1);

		// PCLK1 = HCLK/2
		RCC_PCLK1Config(RCC_HCLK_Div1);

		// PLLCLK = 8MHz * 9 = 72 MHz
		//RCC_PLLConfig(RCC_PLLSource_HSE_Div1, RCC_PLLMul_9);
		RCC_PLLConfig(RCC_PLLSource_HSE_Div1, RCC_PLLMul_3);

		// Enable PLL
		RCC_PLLCmd(ENABLE);

		// Wait till PLL is ready
		while (RCC_GetFlagStatus(RCC_FLAG_PLLRDY) == RESET)
		{
		}

		// Select PLL as system clock source
		RCC_SYSCLKConfig(RCC_SYSCLKSource_PLLCLK);

		// Wait till PLL is used as system clock source
		while (RCC_GetSYSCLKSource() != 0x08)
		{
		}
	}
}

/*******************************************************************************
 * Function Name  : NVIC_Configuration
 * Description    : Configures Vector Table base location.
 * Input          : None
 * Output         : None
 * Return         : None
 *******************************************************************************/
void NVIC_Configuration(void)
{
#ifdef  VECT_TAB_RAM  
	/* Set the Vector Table base location at 0x20000000 */
	NVIC_SetVectorTable(NVIC_VectTab_RAM, 0x0);
#else  /* VECT_TAB_FLASH  */
	/* Set the Vector Table base location at 0x08000000 */
	NVIC_SetVectorTable(NVIC_VectTab_FLASH, 0x0);
#endif
}

/*******************************************************************************
 * Function Name  : Delay
 * Description    : Inserts a delay time.
 * Input          : nCount: specifies the delay time length.
 * Output         : None
 * Return         : None
 *******************************************************************************/
void Delay(vu32 nCount) { for (; nCount != 0; nCount--); }

int sendTCPKey(KEY key) {
	char buf[1];
	switch (key) {
		case KEY_A: buf[0] = KEY_A_BYTE; break;
		case KEY_B: buf[0] = KEY_B_BYTE; break;
		case KEY_C: buf[0] = KEY_C_BYTE; break;
		case KEY_D: buf[0] = KEY_D_BYTE; break;
		case KEY_E: buf[0] = KEY_E_BYTE; break;
		case KEY_F: buf[0] = KEY_F_BYTE; break;
	}
	if (ESP8266_RequestSendData_Blocking(&ESP8266, ESP_Conn, buf, 1) == ESP_OK) {
		return 0;
	}
	return -1;
}

/*******************************************************************************
 * Function Name  : Function to parse the received ula-200 packet
 * Description    : Parses the Packet defined by ULA-200 Protocol
 * Input          : packet: Char Array that holds the input data
 * Output         : None
 * Return         : true if it could be parsed, otherwise false
 *******************************************************************************/
int parsePacket(ESP8266_t* ESP8266, ESP8266_Connection_t* Connection, char *packet)
{
	uint8_t pos = 0; // integer to hold data position
	uint8_t x = 0; // integer to hold current x
	uint8_t y = 0; // integer to hold current y
	uint8_t len = 0; // integer to hold current length
	uint8_t prom = 0; // integer to hold promille value
	packetTimer = millis(); // set packet timer to actual running count
	char buf[128];

	/* ULA-200 Main Command Switch */
	switch (packet[++pos]) {
		case COMMAND_BACKLIGHT:	// Backlight Control
								pos++;
								if (packet[pos] == 0x30) { 			// '0'
									//T6963_Backlight(0);
									//color = hsv_to_rgb(180, s, 0);
									//set_stripe(stripe, color);
									//refresh_stripe(stripe, NR_OF_LEDS);
									return 1;
								} else if (packet[pos] == 0x31) { 	// '1'
									//T6963_Backlight(1);
									//color = hsv_to_rgb(180, s, v);
									//set_stripe(stripe, color);
									//refresh_stripe(stripe, NR_OF_LEDS);
									return 1;
								} else {
									return 0;
								}
								return 1;
		case COMMAND_CLEAR: 	// Clear Command
								T6963_ClearText();
								T6963_TextGoTo(0, 0);
								current_x = 0;
								current_y = 0;
								return 1;
		case COMMAND_REVERSE: 	// Reverse Display Command
								pos++;
								if (packet[pos] == 0x30) { 			// '0'
									T6963_Reverse(false);
								} else if (packet[pos] == 0x31) { 	// '1'
									T6963_Reverse(true);
								} else {
									return 0;
								}
								return 1;
		case COMMAND_VERSION:	// Version Command
								sprintf(buf, "%c%c%c%c",  ver_maj, ver_min, (ver_build >> 8), (ver_build & 0xFF));
								USART1_Puts(buf);
								return 1;
		case COMMAND_POSITION:	// Cursor Position Command
								x = packet[++pos]-0x20;
								y = packet[++pos]-0x20;
								//Serial.println(" ");
								//Serial.print("X="); Serial.println(x);
								//Serial.print("Y="); Serial.println(y);
								//Serial.println(packet[pos+1]);
								if (x >= LCD_WIDTH)
									x = 0;
								if (y >= LCD_HEIGHT)
									y = 0;

								pos++;
								if (packet[pos] == ETX) {
									T6963_TextGoTo(x,y);
									current_x = x;
									current_y = y;
									return 1;
								} else {
									return 0;
								}
								return 0;
								/*

								//Serial.println(packet[pos]);
									if (x < LCD_WIDTH-1) {
										if (y < LCD_HEIGHT-1) {
											lcd.setCursor(x, y);
										}
									}
									return 1;
								} else {
									return 0;
								}
								*/
		case COMMAND_TEXT:		// Text Out Command
								pos++;
								int len = (int)packet[pos++];
								char *out;
								out = (char*)malloc(256); //(len)*sizeof(char));
								int outpos = 0;
								//Serial.print("L=");
								//Serial.println(len);
								for (int c = pos; c < (len+pos); c++) {
									switch ((unsigned char)packet[c]) {
										case 0xFF: out[outpos] = (char)0xA0; break; // filled block
										case 0x01: out[outpos] = (char)0xA1; break; // heart open
										case 0x02: out[outpos] = (char)0xA2; break; // heart filled
										case 0x03: out[outpos] = (char)0xA3; break; // arrow up
										case 0x04: out[outpos] = (char)0xA4; break; // arrow down
										case 0x05: out[outpos] = (char)0xA5; break; // checkbox empty
										case 0x06: out[outpos] = (char)0xA6; break; // checkbox checked
										case 0x07: out[outpos] = (char)0xA7; break; // checkbox gray
										case 0x08: out[outpos] = (char)0xAA; break; // vbar 1 px
										case 0x09: out[outpos] = (char)0xAB; break; // vbar 2 px
										case 0x0A: out[outpos] = (char)0xAC; break; // vbar 3 px
										case 0x0B: out[outpos] = (char)0xAD; break; // vbar 4 px
										case 0x0C: out[outpos] = (char)0xAE; break; // vbar 5 px
										case 0x0D: out[outpos] = (char)0xAF; break; // vbar 6 px
										case 0x0E: out[outpos] = (char)0xB0; break; // hbar 1 px
										case 0x0F: out[outpos] = (char)0xB1; break; // hbar 2 px
										case 0x10: out[outpos] = (char)0xB2; break; // hbar 3 px
										case 0x11: out[outpos] = (char)0xB3; break; // hbar 4 px
										case 0x12: out[outpos] = (char)0xB4; break; // hbar 5 px
										case 0x13: out[outpos] = (char)0xB5; break; // hbar 6 px
										case 0x1A: out[outpos] = (char)0xA9; break; // arrow right
										case 0x1B: out[outpos] = (char)0xA8; break; // arrow left
										case 0x80: out[outpos] = (char)0xB6; break; // bignum char
										case 0x81: out[outpos] = (char)0xB7; break; // bignum char
										case 0x82: out[outpos] = (char)0xB8; break; // bignum char
										case 0x83: out[outpos] = (char)0xB9; break; // bignum char
										case 0x84: out[outpos] = (char)0xBA; break; // bignum char
										case 0x85: out[outpos] = (char)0xBB; break; // bignum char
										case 0x86: out[outpos] = (char)0xBC; break; // bignum char
										case 0x87: out[outpos] = (char)0xBD; break; // bignum char
										case 0x88: out[outpos] = (char)0xBE; break; // degree char

										default: out[outpos] = (char)packet[c];
									}
									outpos++;
									//Serial.println(out[outpos]);
									//out += (char)packet[c];

									current_x++;
									if (packetTimeout())
										return 0;
								}
								out[outpos] = 0x00;
								//char buf[16];
								//sprintf(buf, "0x%02x", packet[(len+pos)]);
								//T6963_WriteString(buf);
								if (packet[(len+pos)] == ETX) {
									//char outbuf[out.length() + 1];
									//out.toCharArray(outbuf, out.length() + 1);
									T6963_WriteString(out);
									free(out);
									return 1;
								} else {
									free(out);
									return 0;
								}
								return 1;
		case COMMAND_VBAR:		// Vertical Bar Command
								//HD44780_vbar(Driver *drvthis, int x, int y, int len, int promille, int options)
								x = packet[++pos]-0x20;
								y = packet[++pos]-0x20;
								len = packet[++pos]-0x20;
								prom = packet[++pos]-0x20;
								return 1;
	}
	return 0;
}

bool packetTimeout(void)
{
	if ((millis() - packetTimer) >= PACKET_TIMEOUT) {
		sendNak();
		sendNak();
		GPIO_SetBits(GPIOC, GPIO_Pin_10);
		return true;
	} else
		return false;
}

void printClocks(void)
{
	RCC_ClocksTypeDef RCC_ClockFreq;
	RCC_GetClocksFreq (&RCC_ClockFreq);
	printf (
	  "Running at following clocks: \r\n  SYSCLK = %ld MHz\r\n  HCLK   = %ld MHz\r\n  PCLK1  = %ld MHz\r\n  PCLK2  = %ld MHz\r\n\r\n",
	  RCC_ClockFreq.SYSCLK_Frequency / 1000000,
	  RCC_ClockFreq.HCLK_Frequency / 1000000,
	  RCC_ClockFreq.PCLK1_Frequency / 1000000,
	  RCC_ClockFreq.PCLK2_Frequency / 1000000);
}

/**
  * @brief  This function handles SysTick Handler.
  * @param  None
  * @retval None
  */
void SysTick_Handler(void)
{
	ESP8266_TimeUpdate(&ESP8266, 1);
}

int sendTCPAck(ESP8266_t* ESP8266, ESP8266_Connection_t* Connection)
{
	char buf[1];
	buf[0] = ACK;
	if (ESP8266_RequestSendData_Blocking(ESP8266, Connection, buf, 1) == ESP_OK) {
		return 0;
	}
	return -1;
}

int sendTCPNak(ESP8266_t* ESP8266, ESP8266_Connection_t* Connection)
{
	char buf[1];
	buf[0] = NAK;
	if (ESP8266_RequestSendData_Blocking(ESP8266, Connection, buf, 1) == ESP_OK) {
		return 0;
	}
	return -1;
}


void ESP8266_Callback_ServerConnectionDataReceived(ESP8266_t* ESP8266, ESP8266_Connection_t* Connection, uint8_t* Buffer, uint8_t Length)
{
	char inChar = 0x00;
	uint8_t pos = 0;

	ESP_Conn = Connection;

	while (pos < Length) {

		// get the new byte:
		inChar = Buffer[pos++];
		//printf("%d %c\r\n", pos-1, inChar);

		// if the new byte is a start text character start the packet receiving
		if (inChar == STX) {
			// add it to the inputString:
			//inputString += inChar;
			//inputString[inputPos] = inChar;
			packetStarted = true;
			tcp_packetTimer = millis();
		}

		if (packetStarted) {
			// check if packet timeout reached
			if ((millis() - tcp_packetTimer) >= PACKET_TIMEOUT) {
				packetStarted = false;
				packetError = true;
				//return;
			}
			// check for special enquiry command
			if (inChar == ENQ) {
				//delay(15);
				// read the next char to identify replacement value
				inChar = Buffer[pos++];
				if (inChar == DC2) {
					// add a 02 to the inputString:
					//inputString += (char)0x02;
					inputString[inputPos] = (char)0x02;
				} else if (inChar == DC3) {
					// add a 03 to the inputString:
					//inputString += (char)0x03;
					inputString[inputPos] = (char)0x03;
				} else if (inChar == NAK) {
					// add a 05 to the inputString:
					//inputString += (char)0x05;
					inputString[inputPos] = (char)0x05;
				//} else {
				//	inputString += (char)0x05;
				//	inputString += inChar;
				}
				if (inputString[1] == COMMAND_TEXT) inputString[2] = inputString[2] - 1;
			} else if (inChar == ETX) {
					// add it to the inputString:
					//inputString += inChar;
					inputString[inputPos] = inChar;
					packetComplete = true;
					packetType = 1;
					packetStarted = false;
					//inputString = "";
					// if not a enquire nor an end text char add it

					// if the incoming character is a end text character, set a flag
					// so the main loop can do further processing
			} else {
				// add it to the inputString:
				//inputString += inChar;
				inputString[inputPos] = inChar;
			}
		}

		inputPos++;
	}
}

void ESP8266_Callback_WifiGotIP(ESP8266_t* ESP8266)
{
	printf("Wifi got an IP address\r\n");

	/* Read that IP from module */
	printf("Grabbing IP status: %d\r\n", ESP8266_GetSTAIP(ESP8266));
	printf("Got IP: %d.%d.%d.%d\r\n", ESP8266->STAIP[0], ESP8266->STAIP[1], ESP8266->STAIP[2], ESP8266->STAIP[3]);
}

#ifdef DEBUG
/*******************************************************************************
 * Function Name  : assert_failed
 * Description    : Reports the name of the source file and the source line number
 *                  where the assert_param error has occurred.
 * Input          : - file: pointer to the source file name
 *                  - line: assert_param error line source number
 * Output         : None
 * Return         : None
 *******************************************************************************/
void assert_failed(u8* file, u32 line)
{
	/* User can add his own implementation to report the file name and line number,
	 ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
	printf("Wrong parameters value: file %s on line %d\r\n", (char*) file,
			(int) line);
	/* Infinite loop */
	while (1)
	{
	}
}
#endif

/******************* (C) COPYRIGHT 2014 Michael Sauer *****END OF FILE****/

