/**
* @file general.h
*
* @brief This file includes the general program settings
*
* @author Michael Sauer <sauer.uetersen@gmail.com>
*
* @date 20.03.2014 - first Version
*
**/
#ifndef _GENERAL_H
#define _GENERAL_H

//--------------------------------------------------------------
// Includes
//--------------------------------------------------------------
#include <string.h>
#include <stdlib.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <stddef.h>
#include <errno.h>
#include <sys/stat.h>
#include <sys/times.h>
#include <sys/unistd.h>

//--------------------------------------------------------------
// Defines
//--------------------------------------------------------------
#define UART1_BAUDRATE  115200    	///< Bluetooth/RS232 Baudrate
#define UART3_BAUDRATE  57600    	///< ESP8266/RS232 Baudrate

#define DEBUG       	           	///< Uncomment for Debug Mode
#define VERBOSE		5				///< Verbose level in debug mode
//#define WATCHDOG_ENABLED			///< Uncomment to enable Watchdog

/* Version Defines (automagically increased on build) */
#define VER_MAJOR 0
#define VER_MINOR 4
#define VER_BUILD 1362

/* Type definitions */
#define boolean _Bool
#define bool	_Bool
#define true	1
#define false	0

/* ULA200 Protocol Constants */
#define STX  0x02
#define ETX  0x03
#define ENQ  0x05
#define ACK  0x06
#define NAK  0x15
#define DC2  0x12
#define DC3  0x13
#define DEL  0x7F

/* ULA200 Protocol Commands */
#define COMMAND_BACKLIGHT 0x68 // h
#define COMMAND_CLEAR 0x6c // l
#define COMMAND_TEXT 0x73 // s
#define COMMAND_VBAR 0x76 // v
#define COMMAND_HBAR 0x62 // b
#define COMMAND_POSITION 0x70 // p
#define COMMAND_LED 0x64 // d
#define COMMAND_REGISTER 0x52 // R
#define COMMAND_KEY 0x74 // t
#define COMMAND_REVERSE 0x75 // u
#define COMMAND_VERSION 0x56 // V

//--------------------------------------------------------------
// Structures
//--------------------------------------------------------------

extern uint32_t SystemCoreClock;

/* Packet processing */
extern char * inputString;         // a string to hold incoming data
extern int inputPos;			// integer to hold data position
extern __IO bool packetComplete;  // whether the packet is complete
extern __IO bool packetStarted;  // whether the packet receiving is started
extern __IO bool packetError; // whether the packet got errors
extern __IO int packetType; // packet type 0 for serial 1 for tcp

#endif // _GENERAL_H
