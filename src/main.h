/**
 * @file main.h
 *
 * @brief This file includes the main variables, structs, ...
 *
 * @author Michael Sauer <sauer.uetersen@gmail.com>
 *
 * @date 30.08.2014 - First Version (Clone of ml_mainunit.h)
 **/

//--------------------------------------------------------------
#ifndef _MAIN_H
#define _MAIN_H

//--------------------------------------------------------------
// Includes
//--------------------------------------------------------------
#include <uart3.h> // uart2 library

#include "stm32f10x.h"
#include "stm32f10x_conf.h"
#include "t6963.h" // t6963 display library
#include "uart1.h" // uart1 library
#include "uart3.h" // uart1 library
#include "adc.h" // adc library
#include "dwt.h" // dwt library
#include "encoder.h" // Rotary encoder library
#include "general.h" // general includes and defines
#include "rgb_lib.h" // ws2812 library
#include "color.h" // color library
#include "esp8266.h" // esp8266 library
#include "keys.h" // key debouncing and processing

typedef enum {
	KEY_A,
	KEY_B,
	KEY_C,
	KEY_D,
	KEY_E,
	KEY_F
} KEY;

void Delay(vu32 nCount);
int sendTCPKey(KEY key);

//--------------------------------------------------------------
// Variables
//--------------------------------------------------------------

//--------------------------------------------------------------
#endif // __MAIN_H
